#! /usr/bin/env python3

import math
import numpy as np
import matplotlib.pyplot as plt
import itertools
import sympy as sym
from abf import makeFancyNames

import abf
from mpl_toolkits.axes_grid1 import make_axes_locatable

from analyticFunctions import analyticFunction
from convergencePlots import generateYLabelFromDiffOperator

def generateMonomialsString2D( order ):
    #monomialsVect = '[ 1, '
    monomialsVect = '[ '
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            coef = math.factorial( max( i - j, j ) )
            monom = 'x**'  + str( i - j ) + ' * ' + 'y**' + str( j ) + ' / ' + str( coef ) + ', '
            monomialsVect += monom
    #remove last comma
    monomialsVect = ' '.join(monomialsVect.split())[:-1]
    monomialsVect += ' ]'
    return monomialsVect

def generateMonomials( x, y ,order ):
    #size = (int)( ( order**2 + 3 * order ) / 2 )
    size = (int)( ( ( order - 1 )**2 + 3 * ( order - 1 ) ) / 2 )
    monomialsVect = np.zeros( size )
    counter = 0
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            coef = math.factorial( max( i - j, j ) )
            monom = x**( i - j ) * y**( j ) / coef
            monomialsVect[ counter ] = monom
            counter += 1
    return monomialsVect

def buildPairMomentumMatrix( dx, dy, h, ABFFunction ):
    X0j = np.array( [generateMonomials( dx / h, dy / h, order + 1 ) ] )
    W0j = np.array( [ABFFunction( dx, dy, h, order + 1) ] )
    Mpair = X0j.T @ W0j
    return Mpair

def buildMomentrumMatrix( x0, y0, xptcs, yptcs, h, order, ABFFunction):
    numberOfPairs = 0
    size = (int)( ( order**2 + 3 * order ) / 2 )
    M = np.zeros( [ size , size ] )
    for p in range( 0, len( xptcs ) ):
        dx = xptcs[ p ] - x0
        dy = yptcs[ p ] - y0
        r = np.sqrt( dx**2 + dy**2 )
        #if r < ( 2 * h ):
        if ( 0.1 * h ) < r < ( 2 * h ):
            M += buildPairMomentumMatrix( dx, dy, h, ABFFunction )
            numberOfPairs += 1
    return M, numberOfPairs

def plotMatrixM_ax( M, ax, fig ):
    Mfiltered = np.copy( M )

    #for i in range(len( Mfiltered ) ):
    #    for j in range( len( Mfiltered[ i ] ) ):
    #            Mfiltered[ i, j ] = np.log10( Mfiltered[ i, j ] )

    Mfiltered[ abs( Mfiltered ) < 1e-12 ] = np.nan
    im = ax.imshow( Mfiltered, cmap='plasma' ) #interpolation='nearest' )
    # colorbar pain
    divider = make_axes_locatable( ax )
    cax = divider.append_axes( "right", size="5%", pad=0.05 )
    fig.colorbar( im, cax = cax )

def plotMatrixM( M, title, outputFileName ):
    fig, ax = plt.subplots( 1, 1, figsize=( 10, 10 ) )
    plotMatrixM_ax( M, ax, fig )
    plt.title( title, fontsize=24, loc='right', x=-0.3 )
    plt.savefig( outputFileName, bbox_inches='tight' )
    plt.close()

def plotCondM( ax, numOfPtcsArray, condMArray, label, marker ):
    #fig, ax = plt.subplots( 1, 1, figsize=( 10, 7 ) )
    ax.plot( numOfPtcsArray, condMArray, label=label, marker=next( marker ), linewidth=2, markersize=9  )
    #plt.savefig( outputFileName, bbox_inches='tight' )

def evalDiffOperator( x0, y0, xptcs, yptcs, M, C, h, order, ABFFunction ):
    try:
        Psi0 = np.linalg.solve( M, C )

        Ld0 = 0
        f0 = analyticFunction( 0, 0, 0, 0 )
        avgSpacing = 0
        nbsCounter = 0
        for p in range( 0, len( xptcs ) ):
            fj = analyticFunction( xptcs[ p ], yptcs[ p ], 0, 0 )
            dx = xptcs[ p ] - x0
            dy = yptcs[ p ] - y0
            avgSpacing += abs( dx ) + abs( dy )
            r = np.sqrt( dx**2 + dy**2 )
            #if  r  < ( 2  ):
            if ( 0.1 * h ) < r < ( 2 * h ): #FIXME: This is added due to ill placed particles in the center
                nbsCounter += 1
                W0j = np.array( [ ABFFunction( dx, dy, h, order + 1 ) ] )[ 0 ]
                Ld0 += ( fj - f0 ) * np.dot( W0j, Psi0 )

        epsFactor = avgSpacing / nbsCounter
    except:
        print(" M is singular matrix! ")
        Ld0 = np.nan

    return Ld0

def analyseSingleOrder( order,
                        hVect,
                        resolutions,
                        testpointsDistribution,
                        ABFFunction,
                        ABFName,
                        outputFolder,
                        testpointsFolder,
                        plotMatrices = True ):
    numOfPtcsSamples = []
    condMarray = []
    detMarray = []
    # arrays to store test derivatives
    dxarray = []; dyarray = []
    dxxarray = []; dyyarray = []; dxyarray = []

    for h in hVect:
        for resolution in resolutions:
            boxEdge = 2. * ( 2. * h )
            s = h / resolution
            numOfPtcsPerDim = ( ( int )( boxEdge / s ) ) + 1
            numOfPtcs = numOfPtcsPerDim ** 2
            testpointsInputFileName = testpointsFolder + f"testpoints_" + testpointsDistribution + f"_h_{h:.3f}_resolution_{resolution:.3f}_n_{numOfPtcs}.csv"
            points = np.genfromtxt( testpointsInputFileName, delimiter=',' )
            xptcs = points[ 0, : ]
            yptcs = points[ 1, : ]
            x0 = 0.
            y0 = 0.
            M, counter = buildMomentrumMatrix( x0, y0, xptcs, yptcs, h, order, ABFFunction )
            condM = np.linalg.cond( M )
            detM = np.linalg.det( M )

            if plotMatrices == True:
                title = f'N = {counter}, cond(M) = {condM:.3E}, det(M) = {detM:.3E}'
                outputPlotName = outputFolder + 'matrices/' + testpointsDistribution + "/" + ABFName + f'_matrix_distribution-' + testpointsDistribution + f"_order_{order}_h_{h:.3f}_resolution_{resolution:.3f}_n_{counter}.png"
                plotMatrixM( M, title, outputPlotName )

            numOfPtcsSamples.append( counter )
            condMarray.append( condM )
            detMarray.append( detM )

            print("Order: ", order, " Resolution: ", resolution, " h: ", h, " numPtcs: ", counter )

            #TODO: pass list of operator as an array and return it as an array, i.e. matrix in this case
            #NOTE: There is explicit cast from sympy.core.numbers.Float to numpy.float64, since we want to
            #      stack the data in numpy array and avoid unsafe cast.
            # Solve df/dx
            Cx = np.zeros( M[ :, 1 ].size )
            Cx[ 0 ] = 1 / h
            dx = ( np.float64 )( evalDiffOperator( x0, y0, xptcs, yptcs, M, Cx, h, order, ABFFunction ) )
            dxarray.append( dx )

            # Solve df/dy
            Cy = np.zeros( M[ :, 1 ].size )
            Cy[ 1 ] = 1 / h
            dy = ( np.float64 )( evalDiffOperator( x0, y0, xptcs, yptcs, M, Cy, h, order, ABFFunction ) )
            dyarray.append( dy )

            # Solve df^2/dx^2
            Cxx = np.zeros( M[ :, 1 ].size )
            Cxx[ 2 ] = 1 / h**2
            dxx = ( np.float64 )( evalDiffOperator( x0, y0, xptcs, yptcs, M, Cxx, h, order, ABFFunction ) )
            dxxarray.append( dxx )

            # Solve df^2/dxdy
            Cxy = np.zeros( M[ :, 1 ].size )
            Cxy[ 3 ] = 1 / h**2
            dxy = ( np.float64 )( evalDiffOperator( x0, y0, xptcs, yptcs, M, Cxy, h, order, ABFFunction ) )
            dxyarray.append( dxy )

            # Solve df^2/dy^2
            Cyy = np.zeros( M[ :, 1 ].size )
            Cyy[ 4 ] = 1 / h**2
            dyy = ( np.float64 )( evalDiffOperator( x0, y0, xptcs, yptcs, M, Cyy, h, order, ABFFunction ) )
            dyyarray.append( dyy )

    return numOfPtcsSamples, condMarray, detMarray, dxarray, dyarray, dxxarray, dxyarray, dyyarray

def plotConditionalNumbers( orderNumOfPtcsArray, resolutions, orderCondMArray, h, distribution ):
    marker = itertools.cycle(('o', 'v', 's', '^', '*', 'D', '>', '<', 'p', 'h', 'X', 'd'))
    fig, ax = plt.subplots( 1, 1, figsize=( 11, 8 ) )
    for i in range( 0, len (orders) ):

        idx_order =  np.argsort( orderNumOfPtcsArray[ i ] )
        orderNumOfPtcsArray[ i ] = orderNumOfPtcsArray[ i ][ idx_order ]
        orderCondMArray[ i ] = orderCondMArray[ i ][ idx_order ]

        label = 'order = ' + str( orders[ i ] )
        plotCondM( ax, orderNumOfPtcsArray[ i ], orderCondMArray[ i ], label, marker )

    #Put this first
    ax_top = ax.twiny()
    ax_top.set_xlim([ resolutions[ 0 ], resolutions[ -1 ]  ])
    ax_top.set_xlabel( r"resolution $h/s$" )

    ax.grid( color='black', linestyle='--', linewidth=0.5 )
    ax.set_ylabel( r'cond$(\mathbb{M}) $ ')
    ax.set_xlabel( r'number of particles $ N $' )
    ax.set_yscale('log')
    leg = ax.legend()
    leg.get_frame().set_edgecolor('k')

    title = f'Distribution: {distribution}, '
    if distribution == 'cartesian-perturbed':
        title += r'$\epsilon/s$: ' + f"{esr}, "
    title += f'h = {h}'
    plt.title( title, fontsize=24 )

    outputPlotName = resultsPath + abfName + '_distribution-' + distribution + f'_h{h}_condM' +  '.png' #TODO: resultsPath not properlly passed
    plt.savefig( outputPlotName, bbox_inches='tight' )
    plt.close()

def plotDeterminants( orderNumOfPtcsArray, resolutions, orderDetMArray, h, distribution ):
    marker = itertools.cycle(('o', 'v', 's', '^', '*', 'D', '>', '<', 'p', 'h', 'X', 'd'))
    fig, ax = plt.subplots( 1, 1, figsize=( 11, 8 ) )
    for i in range( 0, len (orders) ):

        idx_order =  np.argsort( orderNumOfPtcsArray[ i ] )
        orderNumOfPtcsArray[ i ] = orderNumOfPtcsArray[ i ][ idx_order ]
        orderDetMArray[ i ] = orderDetMArray[ i ][ idx_order ]

        label = 'order = ' + str( orders[ i ] )
        plotCondM( ax, orderNumOfPtcsArray[ i ], orderDetMArray[ i ], label, marker )

    ax.grid( color='black', linestyle='--', linewidth=0.5 )
    ax.grid( which="minor", color='grey', linestyle='--', linewidth=0.2 )
    ax.set_ylabel( r'log(det$(\mathbb{M}))$ ')
    ax.set_xlabel( r'number of particles $ N $, $ h $ = ' + f'{h}')
    ax.set_yscale('symlog')
    ax.legend()
    leg = ax.legend()
    leg.get_frame().set_edgecolor('k')
    ticks = plt.yticks()[0]
    plt.yticks(np.delete(ticks, np.arange(1, ticks.size, 2)))

    ax_top = ax.twiny()
    ax_top.set_xlim([ resolutions[ 0 ], resolutions[ -1 ] ])
    ax_top.set_xlabel( r"resolution $h/s$" )

    title = f'Distribution: {distribution}, h = {h}'
    plt.title( title, fontsize=24 )

    #This works only for positive values
    from extra import MinorSymLogLocator
    yaxis = plt.gca().yaxis
    yaxis.set_minor_locator(MinorSymLogLocator.MinorSymLogLocator(1e-1))

    outputPlotName = resultsPath + abfName + '_distribution-' + distribution + f'_h{h}_detM' + '.png' #TODO: resultsPath not properlly passed
    plt.savefig( outputPlotName, bbox_inches='tight' )
    plt.close()

def plotApproximationError( orderNumOfPtcsArray, resolutions, orderLdErrorArray, h, distribution, diffOperator ):
    marker = itertools.cycle(('o', 'v', 's', '^', '*', 'D', '>', '<', 'p', 'h', 'X', 'd'))
    fig, ax = plt.subplots( 1, 1, figsize=( 11, 8 ) )
    for i in range( 0, len (orders) ):

        idx_order =  np.argsort( orderNumOfPtcsArray[ i ] )
        orderNumOfPtcsArray[ i ] = orderNumOfPtcsArray[ i ][ idx_order ]
        orderLdErrorArray[ i ] = orderLdErrorArray[ i ][ idx_order ]

        label = 'order = ' + str( orders[ i ] )
        plotCondM( ax, orderNumOfPtcsArray[ i ], orderLdErrorArray[ i ], label, marker )

    #Put this first
    ax_top = ax.twiny()
    ax_top.set_xlim([ resolutions[ 0 ], resolutions[ -1 ]  ])
    ax_top.set_xlabel( r"resolution $h/s$" )

    ax.grid( color='black', linestyle='--', linewidth=0.5 )
    ax.set_ylabel( generateYLabelFromDiffOperator( diffOperator ) )
    ax.set_xlabel( r'number of particles $ N $' )
    ax.set_yscale('log')
    leg = ax.legend()
    leg.get_frame().set_edgecolor('k')

    title = f'Distribution: {distribution}, '
    if distribution == 'cartesian-perturbed':
        title += r'$\epsilon/s$: ' + f"{esr}, "
    title += f'h = {h}'
    plt.title( title, fontsize=24 )

    outputPlotName = resultsPath + abfName + '_distribution-' + distribution + f'_h{h}_Ld' + diffOperator + 'Error' + '.png' #TODO: resultsPath not properlly passed
    plt.savefig( outputPlotName, bbox_inches='tight' )
    plt.close()


if __name__ == "__main__":
    import os
    import argparse
    argparser = argparse.ArgumentParser(description="Particle sample sets parameters")
    g = argparser.add_argument_group("selection parameters")
    g.add_argument("--distribution", choices=['random', 'cartesian', 'cartesian-perturbed'], default='random', help="particle distribution to generate")
    g.add_argument("--esr", type=float, default=0.5, help="perturbation coefficient for perturbed distributions")
    g.add_argument("--plot", type=bool, default=False, help="plot images of particle samples")
    g = argparser.add_argument_group("parameters of particle configuration")
    #NOTE Use: python generatePoints3D.py -l 0.75 0.85,...
    g.add_argument('--resolutions', nargs='+', default=[ 0.85, 1., 1.1, 1.25, 1.5, 1.75, 2 ], help='resolutions to generate')
    g.add_argument('--h-lengths', nargs='+', default=[ 0.001, 0.005, 0.01, 0.05, 0.1, 0.5 ] , help='scales to generate') #for the test 0.1 was added
    g = argparser.add_argument_group("selection parameters")
    g.add_argument("--abf", choices=[ 'W0Conic', 'hermiteWithWendlandC2', 'hermiteWithGaussian', 'taylorWithGaussian', "taylorWithWendlandC2" ], default='taylorWithWendlandC2', help="kernel functions to generate")
    g.add_argument("--max-order", type=int, default=5, help="order of kernel functions to generate")
    g = argparser.add_argument_group("parameters for the analysis")
    g.add_argument('--orders', nargs='+', default=[ 2, 3, 4, 5 ], help='orders to analyze')
    g.add_argument("--divide-by-h", type=bool, default=False, help="divide monomials vector and rhs by stencil size") #This parameter is used, the normalization is done by default
    g.add_argument("--plot-matrices", type=bool, default=False, help="plot heat maps of momentum matrices")

    args = argparser.parse_args()

    #parse input arguments
    abfName = args.abf
    distribution = args.distribution
    plotOption = args.plot
    esr = args.esr
    resolutions = np.array( args.resolutions, dtype=float )
    hLengths = np.array( args.h_lengths, dtype=float )
    orders = np.array( args.orders, dtype=int )
    listOfDerivativesToCompare = [ "x", "y", "xx", "xy", "yy" ]
    analysisResultsPrefix = r'./results/sources2D/'
    testPointsPathBase = r'./results/particles2D/' + distribution + "/"
    if distribution == "cartesian-perturbed": #TODO: Not nice.
        testPointsPathBase += f"cartesian-perturbed_esr_{esr:.2f}/"
    numberOfSamples = 1

    plotMatrices = args.plot_matrices

    if abfName == "W0Conic":
        abfFunction = abf.W_W0Conic
    elif abfName == "hermiteWithWendlandC2":
        abfFunction = abf.W_hermitePolynomialsWithWendlandC2
    elif abfName == "hermiteWithGaussian":
        abfFunction = abf.W_hermitePolynomialsWithGaussian
    elif abfName == "taylorWithWendlandC2":
        abfFunction = abf.W_taylorMonomialsWithWendlandC2
    elif abfName == "taylorWithGaussian":
        abfFunction = abf.W_taylorMonomialsWithGaussian

    #setup plot properties
    plt.rcParams.update({
      "text.usetex": True,
      "text.latex.preamble" : r"\usepackage{amsfonts}",
      "font.family": "Times",
      "font.serif" : "Times New Roman",
      #"font.size"  : 28,
      "font.size"  : 24,
      "axes.prop_cycle" : plt.cycler( color=plt.cm.plasma( np.linspace( 0, 0.8, len( orders ) ) ) )
    })

    #create folder and subfolder for results
    resultsPath = r'./results/sources2D/' + abfName + "_" + distribution + "/"
    if distribution == 'cartesian-perturbed':
        resultsPath += f"cartesian-perturbed_esr_{esr}/"

    if not os.path.exists( resultsPath ):
        os.makedirs( resultsPath )


    for h in hLengths:
        # create list with size of order to store and averade individual samples
        rows = len( orders )
        columns = len( resolutions )
        averagedNumOfParticlesArrayForEveryOrder = np.zeros( ( rows, columns ) )
        averagedCondMArrayForEveryOrder = np.zeros( ( rows, columns ) )
        averagedDetMArrayForEveryOrder = np.zeros( ( rows, columns ) )

        averagedDxArrayForEveryOrder  = np.zeros( ( rows, columns ) )
        averagedDyArrayForEveryOrder  = np.zeros( ( rows, columns ) )
        averagedDxxArrayForEveryOrder = np.zeros( ( rows, columns ) )
        averagedDxyArrayForEveryOrder = np.zeros( ( rows, columns ) )
        averagedDyyArrayForEveryOrder = np.zeros( ( rows, columns ) )

        # sample the analysis over number of particle samples
        for sample in range( 0, numberOfSamples ):
            testPointsPath = testPointsPathBase + f"sample_{sample}/"
            orderNumOfPtcsArray = []
            orderCondMArray = []
            orderDetMArray = []

            orderDxArray = []; orderDyArray = []
            orderDxxArray = []; orderDxyArray = []; oderdDyyArray = []

            orderNumOfPtcsArray.clear()
            orderCondMArray.clear()
            orderDetMArray.clear()

            orderDxArray.clear(); orderDyArray.clear()
            orderDxxArray.clear(); orderDxyArray.clear(); orderDyyArray = []

            for order_idx in range( 0, len( orders ) ):

                order = orders[ order_idx ]
                numOfPtcsArray, condMArray, detMArray, dxArray, dyArray, \
                        dxxArray, dxyArray, dyyArray = analyseSingleOrder( order,
                                                                           np.array( [ h ] ),
                                                                           resolutions,
                                                                           distribution,
                                                                           abfFunction,
                                                                           abfName,
                                                                           resultsPath,
                                                                           testPointsPath,
                                                                           args.plot_matrices )
                print( "Number of particles: ",  numOfPtcsArray )
                print( "Cond(M): ", condMArray )
                print( "Det(M): ", detMArray )
                print( "Dx(f): ", dxArray ); print( "Dy(f): ", dyArray )
                print( "Dxx(f): ", dxxArray ); print( "Dxy(f): ", dxyArray ); print( "Dxx(f): ", dyyArray )

                # store computed results
                averagedNumOfParticlesArrayForEveryOrder[ order_idx ] += numOfPtcsArray
                averagedCondMArrayForEveryOrder[ order_idx ] += condMArray
                averagedDetMArrayForEveryOrder[ order_idx ] += detMArray

                averagedDxArrayForEveryOrder[ order_idx ] += dxArray
                averagedDyArrayForEveryOrder[ order_idx ] += dyArray
                averagedDxxArrayForEveryOrder[ order_idx ] += dxxArray
                averagedDxyArrayForEveryOrder[ order_idx ] += dxyArray
                averagedDyyArrayForEveryOrder[ order_idx ] += dyyArray

        # Save data    ata arrays
        outputNamePrefix = resultsPath + abfName + '_distribution-' + distribution

        outputNamePoints = outputNamePrefix + f'_h{h}_numberOfPoints.csv'
        outputNameCondMArray = outputNamePrefix + f'_h{h}_condMArray.csv'
        outputNameDetMArray = outputNamePrefix + f'_h{h}_detMArray.csv'
        outputNameDxArray = outputNamePrefix + f'_h{h}_DxArray.csv'
        outputNameDyArray = outputNamePrefix + f'_h{h}_DyArray.csv'
        outputNameDxxArray = outputNamePrefix + f'_h{h}_DxxArray.csv'
        outputNameDxyArray = outputNamePrefix + f'_h{h}_DxyArray.csv'
        outputNameDyyArray = outputNamePrefix + f'_h{h}_DyyArray.csv'

        # while saving the data, average them
        np.savetxt( outputNamePoints,       averagedNumOfParticlesArrayForEveryOrder / numberOfSamples,   delimiter=',' )
        np.savetxt( outputNameCondMArray,   averagedCondMArrayForEveryOrder / numberOfSamples,            delimiter=',' )
        np.savetxt( outputNameDetMArray,    averagedDetMArrayForEveryOrder / numberOfSamples,             delimiter=',' )
        np.savetxt( outputNameDxArray,      averagedDxArrayForEveryOrder / numberOfSamples,               delimiter=',' )
        np.savetxt( outputNameDyArray,      averagedDyArrayForEveryOrder / numberOfSamples,               delimiter=',' )
        np.savetxt( outputNameDxxArray,     averagedDxxArrayForEveryOrder / numberOfSamples,              delimiter=',' )
        np.savetxt( outputNameDxyArray,     averagedDxyArrayForEveryOrder / numberOfSamples,              delimiter=',' )
        np.savetxt( outputNameDyyArray,     averagedDyyArrayForEveryOrder / numberOfSamples,              delimiter=',' )

        print( "saved" )
        #exit()

        # read the averaged data to perform plots
        orderNumOfPtcsArray = np.genfromtxt( outputNamePoints, delimiter=',' )
        orderCondMArray = np.genfromtxt( outputNameCondMArray, delimiter=',' )
        orderDetMArray = np.genfromtxt( outputNameDetMArray, delimiter=',' )
        orderDxArray = np.genfromtxt( outputNameDxArray, delimiter=',' )
        orderDyArray = np.genfromtxt( outputNameDyArray, delimiter=',' )
        orderDxxArray = np.genfromtxt( outputNameDxxArray, delimiter=',' )
        orderDxyArray = np.genfromtxt( outputNameDxyArray, delimiter=',' )
        orderDyyArray = np.genfromtxt( outputNameDyyArray, delimiter=',' )

        plt.rcParams.update({
          "font.size"  : 24
        })

        plotConditionalNumbers( orderNumOfPtcsArray, resolutions, orderCondMArray, h, distribution )
        #plotDeterminants( orderNumOfPtcsArray, resolutions, orderDetMArray, h, distribution )

        #plot approximation operators error
        print( "orderDxArrayMod" )
        print( orderDxArray )

        #for i in range( 0, len( orders ) ):
        #    dxArray = orderDxArray[ i ]
        #    dxAnalytical =

        #Filter the array to remove ill zeros
        print( orderDxArray )
        print( orderDxArray.shape )
        for o in range( 0, len( orders ) ):
            #TODO: Remove this garbage something...
            orderDxDiffArray = [ orderDxArray[ o ][ i ] if abs(orderDxArray[ o ][ i ])> 1e-18 else 0 for i in range( 0, len(orderDxArray[ o ] ) ) ]
            orderDyDiffArray = [ orderDxArray[ o ][ i ] if abs(orderDyArray[ o ][ i ])> 1e-18 else 0 for i in range( 0, len(orderDyArray[ o ] ) ) ]
            orderDxxDiffArray = [ orderDxxArray[ o ][ i ] if abs(orderDxxArray[ o ][ i ])> 1e-18 else 0 for i in range( 0, len(orderDxxArray[ o ] ) ) ]
            orderDxyDiffArray = [ orderDxyArray[ o ][ i ] if abs(orderDxyArray[ o ][ i ])> 1e-18 else 0 for i in range( 0, len(orderDxyArray[ o ] ) ) ]
            orderDyyDiffArray = [ orderDyyArray[ o ][ i ] if abs(orderDyyArray[ o ][ i ])> 1e-18 else 0 for i in range( 0, len(orderDyyArray[ o ] ) ) ]
            for i in range( 0, len( orderDxDiffArray ) ):
                orderDxDiffArray[ i ] = abs( orderDxDiffArray[ i ] - analyticFunction( 0, 0, 1, 0 ) )
                orderDyDiffArray[ i ] = abs( orderDyDiffArray[ i ] - analyticFunction( 0, 0, 0, 1 ) )
                orderDxxDiffArray[ i ] = abs( orderDxxDiffArray[ i ] - analyticFunction( 0, 0, 2, 0 ) )
                orderDxyDiffArray[ i ] = abs( orderDxyDiffArray[ i ] - analyticFunction( 0, 0, 1, 1 ) )
                orderDyyDiffArray[ i ] = abs( orderDyyDiffArray[ i ] - analyticFunction( 0, 0, 0, 2 ) )

            orderDxArray[ o ] = orderDxDiffArray
            orderDyArray[ o ] = orderDyDiffArray
            orderDxxArray[ o ] = orderDxxDiffArray
            orderDxyArray[ o ] = orderDxyDiffArray
            orderDyyArray[ o ] = orderDyyDiffArray
            print( "PROBLEMATIC DERIVATIVE: ", orderDxyDiffArray[ i ], " analytical derivative",  analyticFunction( 0, 0, 1, 1 ) )


        plotApproximationError( orderNumOfPtcsArray, resolutions, orderDxArray, h, distribution, 'x' )
        plotApproximationError( orderNumOfPtcsArray, resolutions, orderDyArray, h, distribution, 'y' )
        plotApproximationError( orderNumOfPtcsArray, resolutions, orderDxxArray, h, distribution, 'xx' )
        plotApproximationError( orderNumOfPtcsArray, resolutions, orderDxyArray, h, distribution, 'xy' )
        plotApproximationError( orderNumOfPtcsArray, resolutions, orderDyyArray, h, distribution, 'yy' )
