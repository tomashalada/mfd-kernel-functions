#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

import matplotlib.mlab as mlab
import matplotlib.colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

import abf

#TODO: Move this to another file
def getOrderFrom1D( index ):
    index +=2

    leftBound = 0
    for i in range( 0, index + 1 ):
        leftBound += 0 + i
        rightBound = leftBound + 1 + i
        if index >= leftBound and index <= rightBound:
           return i

def axsPlot( x, y, z, ax, fig, order, abfName ):
    ax.set_aspect( 1.0 / ax.get_data_ratio(), adjustable='box' )
    contDens = 50

    if abfName == 'W0Conic':
        coefToObtainBetterFigures = 10
        contour = ax.contour( x, y, z * coefToObtainBetterFigures, contDens, levels=np.linspace( -(order**2), (order**2), 45 ), cmap='plasma' ) #linewidths=0.5
        norm = matplotlib.colors.Normalize( vmin = contour.cvalues.min(), vmax = contour.cvalues.max() )
    elif abfName == "hermiteWithGaussian":
        contour = ax.contour( x, y, z, 100,  cmap='plasma' ) #linewidths=0.5
        norm = matplotlib.colors.Normalize( vmin = contour.cvalues.min(), vmax = contour.cvalues.max() )

    sm = plt.cm.ScalarMappable( norm = norm, cmap = contour.cmap )
    sm.set_array([])
    divider = make_axes_locatable( ax )
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar( sm, cax = cax )

def axsPlotLogAbs( x, y, z, ax, fig, order, abfName ):
    ax.set_aspect( 1.0 / ax.get_data_ratio(), adjustable='box' )
    contDens = 50

    if abfName == 'W0Conic':
        contour = ax.contour( x, y, np.log( abs( z ) ), 100,  cmap='plasma' ) #linewidths=0.5
        norm = matplotlib.colors.Normalize( vmin = contour.cvalues.min(), vmax = contour.cvalues.max() )
    elif abfName == "hermiteWithGaussian":
        contour = ax.contour( x, y, np.log( abs( z ) ), 100,  cmap='plasma' ) #linewidths=0.5
        norm = matplotlib.colors.Normalize( vmin = contour.cvalues.min(), vmax = contour.cvalues.max() )

    sm = plt.cm.ScalarMappable( norm = norm, cmap = contour.cmap )
    sm.set_array([])
    divider = make_axes_locatable( ax )
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar( sm, cax = cax )

#TODO: There is too many duplicit lines.
def plotAbf( radius, h, order, abfFunction, abfName ):
    x = np.linspace( -radius, radius, 100 )
    y = np.linspace( -radius, radius, 100 )
    X, Y = np.meshgrid( x, y )
    W = abfFunction( X, Y, h, order )

    for i in range( 0, len( W ) ):
        curentOrder =  getOrderFrom1D( i )
        print( f"Plotting order {curentOrder}, element{i}." )

        #regular plot
        fig, axs = plt.subplots( 1, 1, figsize=( 10, 10 ) )
        axsPlot( X, Y, W[ i ], axs, fig, curentOrder, abfName )
        anotate = 'p = ' + str( i + 1 )
        axs.text(.10,.94, anotate, color='black', bbox=dict(facecolor='white', edgecolor='black', pad=10.0), horizontalalignment='center', transform=axs.transAxes, fontsize=26 )
        name = resultsPath + abfName + '_contour_p' + str( i ) + '.png'
        plt.savefig( name, bbox_inches='tight' )
        plt.close()

        #logaritmic plot
        fig, axs = plt.subplots( 1, 1, figsize=( 10, 10 ) )
        axsPlotLogAbs( X, Y, W[ i ], axs, fig, curentOrder, abfName )
        anotate = 'p = ' + str( i + 1 )
        axs.text(.10,.94, anotate, color='black', bbox=dict(facecolor='white', edgecolor='black', pad=10.0), horizontalalignment='center', transform=axs.transAxes, fontsize=26 )
        name = resultsPath + abfName + '_contourLogAbs_p' + str( i ) + '.png'
        plt.savefig( name, bbox_inches='tight' )
        plt.close()

if __name__ == "__main__":
    import os
    import argparse
    argparser = argparse.ArgumentParser(description="Plot contours of kernel functions")
    g = argparser.add_argument_group("selection parameters")
    g.add_argument("--abf", choices=[ 'W0Conic', 'hermiteWithWendlandC2', 'hermiteWithGaussian' ], default='W0Conic', help="kernel functions to generate")
    g.add_argument("--order", type=int, default=5, help="order of kernel functions to generate")

    #parse input arguments
    args = argparser.parse_args()
    abfName = args.abf
    order = args.order
    h = 1
    radius = 2

    if abfName == "W0Conic":
        abfFunction = abf.W_W0Conic
    elif abfName == "hermiteWithWendladnC2":
        abfFunction = abf.W_hermitePolynomialsWithWendlandC2
    elif abfName == "hermiteWithGaussian":
        abfFunction = abf.W_hermitePolynomialsWithGaussian

    #setup plot parameters
    plt.rcParams.update({
      "text.usetex": True,
      "font.family": "Times",
      "font.serif" : "Times New Roman",
      "font.size"  : 20
    })

    #create folder and subfolder for results
    resultsPath = r'./abfFigures/' + abfName + "/"
    if not os.path.exists( resultsPath ):
        os.makedirs( resultsPath )

    #plot the functions
    plotAbf(radius, h, order, abfFunction, abfName)
