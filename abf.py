import math
import numpy as np

def makeFancyNames( abfName ):
    abfFancyName = ""
    # First, decode ABFs:
    if "taylor" in abfName:
        abfFancyName += "Taylor mon."
    elif "hermite" in abfName:
        abfFancyName += "Hermite pol."
    # Decode weight function:
    if "WendlandC2" in abfName:
        abfFancyName += " + WendlandC2"
    elif "Gaussian" in abfName:
        abfFancyName += " + Gaussian"
    return abfFancyName

"""
RBF + TaylorMonomials:
ABF generated as Taylor monomials multiplied by arbitrary RBF.
"""

def generateTaylorMonomialsString( order, includeZerothOrder = False ):
    monomialsVect = '[ '
    if includeZerothOrder == True:
        monomialsVect += '1, '
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            coef = math.factorial( max( i - j, j ) )
            monom = 'x**'  + str( i - j ) + ' * ' + 'y**' + str( j ) + ' / ' + str( coef ) + ', '
            monomialsVect += monom
    #remove last comma
    monomialsVect = ' '.join( monomialsVect.split() )[ :-1 ]
    monomialsVect += ' ]'
    return monomialsVect

def generateTaylorMonomials( x, y ,order ):
    #size = (int)( ( order**2 + 3 * order ) / 2 )
    size = (int)( ( ( order - 1 )**2 + 3 * ( order - 1 ) ) / 2 )
    monomialsVect = np.zeros( size )
    counter = 0
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            coef = math.factorial( max( i - j, j ) )
            monom = x**( i - j ) * y**( j ) / coef
            monomialsVect[ counter ] = monom
            counter += 1
    return monomialsVect

def RBFGaussian( r, h, normalize = True ):
    q = r / h

    if normalize == True:
        coef2D = 1 / ( np.pi * h**2 )
    else:
        coef2D = 1
    val = coef2D * np.exp(-q * q )
    return val

def W_taylorMonomialsWithGaussian( x, y, h, order, includeZerothOrder = False ):
    taylorMonomials = generateTaylorMonomialsString( order, includeZerothOrder )
    Xji = np.array(  eval( taylorMonomials, { 'x': x / h, 'y': y / h } ) , dtype=np.float64 )
    r = np.sqrt( x**2 + y**2 )
    W = Xji  * RBFGaussian( r, h )
    return W

"""
PDR of RBF: W0 conic
ABF generated from partial derivatives of simple cone.
"""

def generateConicABFMonomialString( order, includeZerothOrder = False  ):
    import sympy as sym
    monomialsVect = '[ '
    if includeZerothOrder == True:
        monomialsVect += '1, '
    x , y = sym.symbols( 'x y' )
    f =  ( 1 - ( x**2 + y**2 )**0.5 / 2  )
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            monom = str(  sym.simplify( sym.factor( f.diff( ( x, i - j ), ( y, j ) ) ) ) ) + ', '
            monomialsVect += monom
    #remove last comma
    monomialsVect = ' '.join( monomialsVect.split() )[ :-1 ]
    monomialsVect += ' ]'
    return monomialsVect

def W_W0Conic( x, y, h, order, normalize = False, includeZerothOrder = False  ):
    W0derivatives = generateConicABFMonomialString( order, includeZerothOrder )

    if normalize == True:
        W0const = - 3 / ( 4 * np.pi * h )
    else:
        W0const = 1

    W = W0const * np.array( eval( W0derivatives, { 'x': x, 'y': y } ), dtype=np.float64 )
    return W

"""
RBF + Hermite polynomials
ABF generatedas Hermite polynomials multiplied by arbitrary RBF
"""

def RBFWendlandC2( r, h = 1, normalize = True ):
    q = r / h

    if normalize == True:
        coef2D = 0.03482 / ( h * h ) # 7/(4*PI*16*h^2)*(5/8)
    else:
        coef2D = 1

    val = coef2D * ( 1. + 2. * q) * ( 2. - q ) * ( 2. - q ) * ( 2. - q ) * ( 2. - q )
    return val

def W_hermitePolynomialsWithGaussian( x, y, h, order ):
    from scipy import special

    Wji = []
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            Hi = special.hermite( j )
            Hj = special.hermite( i - j )
            r = np.sqrt( x**2 + y**2 )
            Wji_loc = Hi( x / np.sqrt( 2 ) ) * Hj( y / np.sqrt( 2 ) ) / np.sqrt( 2**( i + j ) ) * RBFWendlandC2( r, h )
            Wji.append( Wji_loc )
    W = np.array( Wji )
    return W

def W_hermitePolynomialsWithWendlandC2( x, y, h, order ):
    from scipy import special
    Wji = []
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            Hi = special.hermite( j )
            Hj = special.hermite( i - j )
            a = j
            b = i - j
            r = np.sqrt( x**2 + y**2 )
            Wji_loc = Hi( x / ( np.sqrt( 2 ) * h ) ) * Hj( y / ( np.sqrt( 2 ) * h ) ) / np.sqrt( 2**( a + b ) ) * RBFWendlandC2( r, h )
            Wji.append( Wji_loc )
    W = np.array( Wji )
    return W

# NOTE: This is example how it shoul be done
def W_taylorMonomialsWithWendlandC2( x, y, h, order, includeZerothOrder = False ):
    #taylorMonomials = generateTaylorMonomialsString( order, includeZerothOrder )
    #Xji = np.array(  eval( taylorMonomials, { 'x': x / h, 'y': y / h } ) , dtype=np.float64 )
    Xji  = np.array(  generateTaylorMonomials( x / h, y / h, order )  )
    r = np.sqrt( x**2 + y**2 )
    W = Xji  * RBFWendlandC2( r, h )
    return W
