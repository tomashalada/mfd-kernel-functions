#!/bin/sh

ordersList="2 4 6 8 10"
resolutionsList="0.85 1. 1.1 1.25 1.5 1.75 2 2.25 2.5 2.75"
hList="0.001 0.005 0.01 0.05 0.1 0.5 1.0"

##  Generate all interesting distributions
#./generatePoints.py --distribution=random --resolutions $resolutionsList --h-lengths $hList
#./generatePoints.py --distribution=cartesian --resolutions $resolutionsList --h-lengths $hList
#./generatePoints.py --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList --h-lengths $hList
#./generatePoints.py --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList --h-lengths $hList
#./generatePoints.py --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList --h-lengths $hList

#------------------------------------------------------------------------------------------------------------------------------
# Test individual ABFs: taylorWithWendlandC2
#------------------------------------------------------------------------------------------------------------------------------

#./analysis2D.py --abf=taylorWithWendlandC2 --distribution=random --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=taylorWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList

#./convergencePlots.py -convergence-plots --abf=taylorWithWendlandC2 --distribution=random --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=taylorWithWendlandC2 --distribution=cartesian --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList

#------------------------------------------------------------------------------------------------------------------------------
# Test individual ABFs: taylorWithGaussiandC2
#------------------------------------------------------------------------------------------------------------------------------

#./analysis2D.py --abf=taylorWithGaussian --distribution=random --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=taylorWithGaussian --distribution=cartesian --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList

#./convergencePlots.py -convergence-plots --abf=taylorWithGaussian --distribution=random --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=taylorWithGaussian --distribution=cartesian --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.25 --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.5 --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.75 --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList

#------------------------------------------------------------------------------------------------------------------------------
# Test individual ABFs: hermiteWithWendlandC2
#------------------------------------------------------------------------------------------------------------------------------

#./analysis2D.py --abf=hermiteWithWendlandC2 --distribution=random --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=hermiteWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./analysis2D.py --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList

#./convergencePlots.py -convergence-plots --abf=hermiteWithWendlandC2 --distribution=random --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=hermiteWithWendlandC2 --distribution=cartesian --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList
#./convergencePlots.py -convergence-plots --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolution=2.5 --resolutions $resolutionsList --orders $ordersList --h-lengths $hList

#------------------------------------------------------------------------------------------------------------------------------
# For certain resolutions, compare different abfs
#------------------------------------------------------------------------------------------------------------------------------

#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=random --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=random --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList --orders $ordersList --resolution=2.5

#------------------------------------------------------------------------------------------------------------------------------
# For certain abf, compare different distributions
#------------------------------------------------------------------------------------------------------------------------------
# NOTE: For simplicity and readable diagrams, we compare separately cartesian pertuberd distrbutions and "almost" cartesian
#       resolution with completly random distribution

#./convergencePlots.py -compare-distributions --abf=taylorWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./convergencePlots.py -compare-distributions --abf=taylorWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList --orders $ordersList --resolution=2.5

#./convergencePlots.py -compare-distributions --abf=taylorWithGaussian --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./convergencePlots.py -compare-distributions --abf=taylorWithGaussian --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList --orders $ordersList --resolution=2.5

#./convergencePlots.py -compare-distributions --abf=hermiteWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./convergencePlots.py -compare-distributions --abf=hermiteWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList --orders $ordersList --resolution=2.5

#------------------------------------------------------------------------------------------------------------------------------
# Compare condition numbers for different abfs and given distribution
#------------------------------------------------------------------------------------------------------------------------------

#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=random --resolutions $resolutionsList  --orders $ordersList --resolution=2.5
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList  --orders $ordersList --resolution=2.5
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList --orders $ordersList --resolution=2.5

#------------------------------------------------------------------------------------------------------------------------------
# Compare condition number for different distributions and given abf
#------------------------------------------------------------------------------------------------------------------------------

#./analysis2D.py -compare-cond-distribution --abf=taylorWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./analysis2D.py -compare-cond-distribution --abf=taylorWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#
#./analysis2D.py -compare-cond-distribution --abf=taylorWithGaussian --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./analysis2D.py -compare-cond-distribution --abf=taylorWithGaussian --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#
#./analysis2D.py -compare-cond-distribution --abf=hermiteWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList --orders $ordersList --resolution=2.5
#./analysis2D.py -compare-cond-distribution --abf=hermiteWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList --orders $ordersList --resolution=2.5
