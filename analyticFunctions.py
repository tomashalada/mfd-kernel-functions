import sympy as sym

def analyticFunction( x, y, diff_x = 0, diff_y = 0 ):
    xs, ys = sym.symbols( 'xs ys' )
    x_mod = x - 0.3423
    y_mod = y - 0.123
    f = 1 + ( xs * ys )**4 + \
        ( xs + ys ) + \
        ( xs**2 + ys**2 ) + \
        ( xs**3 + ys**3 ) + \
        ( xs**4 + ys**4 ) + \
        ( xs**5 + ys**5 ) + \
        ( xs**6 + ys**6 )
    if diff_x == 0 and diff_y == 0:
        return f.subs( [ (xs, x), (ys, y ) ] )
    else:
        df = f.diff( ( xs, diff_x ), ( ys, diff_y ) )
        return df.subs( [ (xs, x), (ys, y ) ] )

def analyticalFunctionWrapper( x, y, diffOperator ):
    if diffOperator == "x":
        ret = analyticFunction( 0, 0, 1, 0 )
    elif diffOperator == "y":
        ret = analyticFunction( 0, 0, 0, 1 )
    elif diffOperator == "xx":
        ret = analyticFunction( 0, 0, 2, 0 )
    elif diffOperator == "xy":
        ret = analyticFunction( 0, 0, 1, 1 )
    elif diffOperator == "yy":
        ret = analyticFunction( 0, 0, 0, 2 )
    else:
        exit( f"analyticalFunctions.py - analyticalFunctionWrapper: Invalid differential operator {diffOperator}." )

    return ret
