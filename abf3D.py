import math
import numpy as np

"""
RBF + TaylorMonomials:
ABF generated as Taylor monomials multiplied by arbitrary RBF.
"""

def generateTaylorMonomialsString( order, includeZerothOrder = False ):
    monomialsVect = '[ '
    if includeZerothOrder == True:
        monomialsVect += '1, '
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            for k in range( 0, i -j + 1 ):
                #coef = math.factorial( max( i - j, j ) )
                coef = math.factorial( max( i - j - k, j, k ) )
                #monom = 'x**'  + str( i - j - k ) + ' * ' + 'y**' + str( j )  + ' * ' + 'z**' + str( k ) + ' / ' + str( coef ) + ', '
                monom = 'x**'  + str( i - j - k ) + ' * ' + 'y**' + str( k )  + ' * ' + 'z**' + str( j ) + ' / ' + str( coef ) + ', '
                monomialsVect += monom
    #remove last comma
    monomialsVect = ' '.join( monomialsVect.split() )[ :-1 ]
    monomialsVect += ' ]'
    return monomialsVect

def RBFGaussian( r, h ):
    q = r / h
    #coef3D = 1 / ( np.pi**( 3 / 2 ) * h**3 )
    coef3D = 1
    val = coef3D * np.exp(-q * q )
    return val

def W_taylorMonomialsWithGaussian( x, y, z, h, order, includeZerothOrder = False ):
    taylorMonomials = generateTaylorMonomialsString( order, includeZerothOrder )
    Xji = np.array(  eval( taylorMonomials, { 'x': x / h, 'y': y / h, 'z': z / h } ) , dtype=np.float64 )
    r = np.sqrt( x**2 + y**2 + z**2 )
    W = Xji  * RBFGaussian( r, h )
    return W

"""
PDR of RBF: W0 conic
ABF generated from partial derivatives of simple cone.
"""

def generateConicABFMonomialString( order, includeZerothOrder = False  ):
    import sympy as sym
    monomialsVect = '[ '
    if includeZerothOrder == True:
        monomialsVect += '1, '
    x , y, z = sym.symbols( 'x y z' )
    f =  ( 1 - ( x**2 + y**2 + z**2 )**0.5 / 2  )
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            for k in range( 0, i -j + 1 ):
                monom = str( f.diff( ( x, i - j ), ( y, k ), ( z, j ) ) ) + ', '
                monomialsVect += monom
    #remove last comma
    monomialsVect = ' '.join( monomialsVect.split() )[ :-1 ]
    monomialsVect += ' ]'
    return monomialsVect

def W_W0Conic( x, y, z, h, order, includeZerothOrder = False  ):
    W0derivatives = generateConicABFMonomialString( order, includeZerothOrder )
    #W0const = - 3 / ( 4 * np.pi * h )
    W0const = - 3 /  4 * np.pi * h
    W = W0const * np.array( eval( W0derivatives, { 'x': x, 'y': y, 'z': z } ), dtype=np.float64 )
    return W

"""
RBF + Hermite polynomials
ABF generatedas Hermite polynomials multiplied by arbitrary RBF
"""


def RBFWendlandC2( r, h = 1 ):
    q = r / h
    #coef3D = 21 / ( 16 * np.pi * h**3 )
    coef3D = 1
    val = coef3D * ( 1 - q / 2 )**4 * ( 2 * q + 1 )
    return val

#: for k in range( 0, i -j + 1 ):
#:     #coef = math.factorial( max( i - j, j ) )
#:     coef = math.factorial( max( i - j - k, j, k ) )
#:     #monom = 'x**'  + str( i - j - k ) + ' * ' + 'y**' + str( j )  + ' * ' + 'z**' + str( k ) + ' / ' + str( coef ) + ', '
#:     monom = 'x**'  + str( i - j - k ) + ' * ' + 'y**' + str( k )  + ' * ' + 'z**' + str( j ) + ' / ' + str( coef ) + ', '
#:     monomialsVect += monom


def W_hermitePolynomialsWithGaussian( x, y, z, h, order ):
    from scipy import special

    Wji = []
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            for k in range( 0, i -j + 1 ):
                Hx = special.hermite( i - j - k )
                Hy = special.hermite( k )
                Hz = special.hermite( j )
                a = i - j - k; b = k; c = k

                r = np.sqrt( x**2 + y**2 + z**2 )
                Wji_loc = Hx( x / ( np.sqrt( 2 ) * h ) ) * Hy( y / ( np.sqrt( 2 ) * h ) ) * Hz( z / ( np.sqrt( 2 ) * h ) ) / np.sqrt( 2**( a + b + c ) ) * RBFGaussian( r, h )
                Wji.append( Wji_loc )

    W = np.array( Wji )

    print( type( W ) )
    print( W.size )
    print( W.shape )

    return W

def W_hermiteWithWendlandC2( x, y, z, h, order ):
    from scipy import special

    Wji = []
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            for k in range( 0, i -j + 1 ):
                Hx = special.hermite( i - j - k )
                Hy = special.hermite( k )
                Hz = special.hermite( j )
                a = i - j - k; b = k; c = k

                r = np.sqrt( x**2 + y**2 + z**2 )
                Wji_loc = Hx( x / ( np.sqrt( 2 ) * h ) ) * Hy( y / ( np.sqrt( 2 ) * h ) ) * Hz( z / ( np.sqrt( 2 ) * h ) ) / np.sqrt( 2**( a + b + c ) ) * RBFWendlandC2( r, h )
                Wji.append( Wji_loc )

    W = np.array( Wji )

    print( type( W ) )
    print( W.size )
    print( W.shape )

    return W
