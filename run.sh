#!/bin/sh

resolutionsList="0.6 0.75 0.85 1. 1.1 1.25 1.5 1.75 2"

# Generate all interesting distributions
#./generatePoints.py --distribution=random --resolutions $resolutionsList
#./generatePoints.py --distribution=cartesian --resolutions $resolutionsList
#./generatePoints.py --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./generatePoints.py --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList --samples-count=10
#./generatePoints.py --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList

#------------------------------------------------------------------------------------------------------------------------------
# Test individual ABFs: taylorWithWendlandC2
#------------------------------------------------------------------------------------------------------------------------------

#./analysis2D.py --abf=taylorWithWendlandC2 --distribution=random --resolutions $resolutionsList
#./analysis2D.py --abf=taylorWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList
#./analysis2D.py --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./analysis2D.py --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList
#./analysis2D.py --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList

#./convergencePlots.py -convergence-plots --abf=taylorWithWendlandC2 --distribution=random --resolutions $resolutionsList
#./convergencePlots.py -convergence-plots --abf=taylorWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList
#./convergencePlots.py -convergence-plots --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./convergencePlots.py -convergence-plots --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList
#./convergencePlots.py -convergence-plots --abf=taylorWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList

#------------------------------------------------------------------------------------------------------------------------------
# Test individual ABFs: taylorWithGaussiandC2
#------------------------------------------------------------------------------------------------------------------------------

#./analysis2D.py --abf=taylorWithGaussian --distribution=random --resolutions $resolutionsList
#./analysis2D.py --abf=taylorWithGaussian --distribution=cartesian --resolutions $resolutionsList
#./analysis2D.py --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./analysis2D.py --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList
#./analysis2D.py --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList

#./convergencePlots.py -convergence-plots --abf=taylorWithGaussian --distribution=random --resolutions $resolutionsList
#./convergencePlots.py -convergence-plots --abf=taylorWithGaussian --distribution=cartesian --resolutions $resolutionsList
#./convergencePlots.py -convergence-plots --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./convergencePlots.py -convergence-plots --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList
#./convergencePlots.py -convergence-plots --abf=taylorWithGaussian --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList

#------------------------------------------------------------------------------------------------------------------------------
# Test individual ABFs: hermiteWithWendlandC2
#------------------------------------------------------------------------------------------------------------------------------

#./analysis2D.py --abf=hermiteWithWendlandC2 --distribution=random --resolutions $resolutionsList
#./analysis2D.py --abf=hermiteWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList
#./analysis2D.py --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./analysis2D.py --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList
#./analysis2D.py --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList

#./convergencePlots.py -convergence-plots --abf=hermiteWithWendlandC2 --distribution=random --resolutions $resolutionsList
#./convergencePlots.py -convergence-plots --abf=hermiteWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList
#./convergencePlots.py -convergence-plots --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./convergencePlots.py -convergence-plots --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions ${resolutionsList}
#./convergencePlots.py -convergence-plots --abf=hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList

#------------------------------------------------------------------------------------------------------------------------------
# For certain resolutions, compare different abfs
#------------------------------------------------------------------------------------------------------------------------------

#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=random --resolutions $resolutionsList
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList

#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=random --resolutions $resolutionsList
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList
#./convergencePlots.py -compare-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList

#------------------------------------------------------------------------------------------------------------------------------
# For certain abf, compare different distributions
#------------------------------------------------------------------------------------------------------------------------------
# NOTE: For simplicity and readable diagrams, we compare separately cartesian pertuberd distrbutions and "almost" cartesian
#       resolution with completly random distribution

#./convergencePlots.py -compare-distributions --abf=taylorWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList
#./convergencePlots.py -compare-distributions --abf=taylorWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList
#
#./convergencePlots.py -compare-distributions --abf=taylorWithGaussian --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList
#./convergencePlots.py -compare-distributions --abf=taylorWithGaussian --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList
#
#./convergencePlots.py -compare-distributions --abf=hermiteWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList
#./convergencePlots.py -compare-distributions --abf=hermiteWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList

#------------------------------------------------------------------------------------------------------------------------------
# Compare condition numbers for different abfs and given distribution
#------------------------------------------------------------------------------------------------------------------------------

#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=random --resolutions $resolutionsList
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian --resolutions $resolutionsList
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList

#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.25 --resolutions $resolutionsList
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.5 --resolutions $resolutionsList
#./analysis2D.py -compare-cond-abfs --abfs taylorWithWendlandC2 taylorWithGaussian hermiteWithWendlandC2 --distribution=cartesian-perturbed --esr=0.75 --resolutions $resolutionsList

#------------------------------------------------------------------------------------------------------------------------------
# Compare condition number for different distributions and given abf
#------------------------------------------------------------------------------------------------------------------------------

#./analysis2D.py -compare-cond-distribution --abf=taylorWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList
#./analysis2D.py -compare-cond-distribution --abf=taylorWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList
#
#./analysis2D.py -compare-cond-distribution --abf=taylorWithGaussian --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList
#./analysis2D.py -compare-cond-distribution --abf=taylorWithGaussian --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList
#
#./analysis2D.py -compare-cond-distribution --abf=hermiteWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 cartesian-perturbed_esr_0.5 cartesian-perturbed_esr_0.75 --resolutions $resolutionsList
#./analysis2D.py -compare-cond-distribution --abf=hermiteWithWendlandC2 --distributions cartesian-perturbed_esr_0.25 random --resolutions $resolutionsList
