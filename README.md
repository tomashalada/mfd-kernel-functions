
****The analysis script includes following files:****

```
mfd-kernel-functions
├── README.md
├── abf2D.py                  - definitions of ABFs (weight functions)
├── abf3D.py                  - definitions of ABFs (weight functions) in 3D ([TODO]: not done yet)
├── analysis2D.py             - analysis procedures including MFD implementation,
│                               this includes plots of errors and cond. number for given particle sample
├── analysis3D.py             - analysis procedures including MFD implementation in 3D ([TODO]: not done yet),
│                               this includes plots of errors and cond. number for given particle sample
├── analyticalFunctions.py    - definitions analytical functions to test the error
├── generatePoints2D.py       - definitions of particles spatial resolutions
├── generatePoints3D.py       - definitions of particles spatial resolutions in 3D
├── convergencePlots.py       - plot all the main results and comparisons from the data obtained by analysis2D/3D.py
│                               this produces the main results - all the convergence plots, comparison of
│                               different resolutions including comparison of condition numbers
│                               [NOTE]: The name is not optional, and all the plots from the analysis should be included here.
├── plotAbf.py                - plot how the ABFs looks like
├── run.sh                    - run all the scripts above with given configurations (by default orders 2 - 5)
└── runHighOrder.sh           - run all the scripts above with confifigurations (by default orders 2 - 10)
```

****The analysis generates results with following structure:****

```
mfd-kernel-functions
└── results
    ├── convergenceAndComparison2D/3D
    │   └── ...               - convergence plots and comparison of different configurations
    ├── particle2D/3D
    │   └── ...               - generated particle configurations used for testing
    ├── results.html          - results summary ([TODO]: not automatically generated yet)
    └── sources2D/3D
        └── ...               - results generated from analysis2D/3D.py script,
                                used as source data for convergencePlots.py script
```
