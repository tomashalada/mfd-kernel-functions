import math
import numpy as np
import matplotlib.pyplot as plt
import itertools


import ABF3D
from mpl_toolkits.axes_grid1 import make_axes_locatable

def generateMonomialsString( order, includeZerothOrder = False ):
    monomialsVect = '[ '
    if includeZerothOrder == True:
        monomialsVect += '1, '
    for i in range( 1, order ):
        for j in range( 0, i + 1 ):
            for k in range( 0, i -j + 1 ):
                #coef = math.factorial( max( i - j, j ) )
                coef = math.factorial( max( i - j - k, j, k ) )
                #monom = 'x**'  + str( i - j - k ) + ' * ' + 'y**' + str( j )  + ' * ' + 'z**' + str( k ) + ' / ' + str( coef ) + ', '
                monom = 'x**'  + str( i - j - k ) + ' * ' + 'y**' + str( k )  + ' * ' + 'z**' + str( j ) + ' / ' + str( coef ) + ', '
                monomialsVect += monom
    #remove last comma
    monomialsVect = ' '.join( monomialsVect.split() )[ :-1 ]
    monomialsVect += ' ]'
    return monomialsVect

def evaluateXij( x, y, z, order ):
    monomials3D = generateMonomialsString( order )
    Xji = np.array( eval( monomials3D, { 'x': x, 'y': y , 'z' : z } ), dtype=np.float64 )
    return Xji

def buildPairMomentumMatrix( dx, dy, dz, h, ABFFunction ):
    X0j = np.array( [evaluateXij( dx / h, dy / h, dz / h, order ) ] )
    W0j = np.array( [ABFFunction( dx, dy, dz, h, order ) ] )
    print( X0j )
    print( X0j.size )
    print( X0j.shape )
    print( type(X0j) )
    print( W0j )
    print( W0j.size )
    print( W0j.shape )
    print( type(W0j) )
    Mpair = X0j.T @ W0j
    print( "Loc matrix cond", np.linalg.cond( Mpair )  )
    print( "Loc matrix det", np.linalg.det( Mpair )  )
    print( Mpair )
    return Mpair

def buildMomentrumMatrix( x0, y0, z0, xptcs, yptcs, zptcs, h, order, ABFFunction ):
    #size = np.sum( np.arange( 1, order + 1 ) )
    #size = np.sum( np.arange( 1, order + 1 ) )
    size = ( np.array( [evaluateXij( x0, y0, z0, order ) ] ) ).size
    print( size )
    M = np.zeros( [ size , size ] )
    for p in range( 0, len( xptcs ) ):
        dx = xptcs[ p ] - x0
        dy = yptcs[ p ] - y0
        dz = zptcs[ p ] - z0
        r = np.sqrt( dx**2 + dy**2 + dz**2 )
        if r < ( 2 * h ):
            M += buildPairMomentumMatrix( dx, dy, dz, h, ABFFunction )
    #print( M )
    return M

def plotMatrixM_ax( M, ax, fig ):
    Mfiltered = np.copy( M )

    #for i in range(len( Mfiltered ) ):
    #    for j in range( len( Mfiltered[ i ] ) ):
    #            Mfiltered[ i, j ] = np.log10( Mfiltered[ i, j ] )

    Mfiltered[ abs( Mfiltered ) < 1e-12 ] = np.nan

    print(Mfiltered)
    im = ax.imshow( Mfiltered, cmap='plasma' ) #interpolation='nearest' )
    # colorbar pain
    divider = make_axes_locatable( ax )
    cax = divider.append_axes( "right", size="5%", pad=0.05 )
    fig.colorbar( im, cax = cax )

def plotMatrixM( M, title, outputFileName ):
    fig, ax = plt.subplots( 1, 1, figsize=( 10, 10 ) )
    plotMatrixM_ax( M, ax, fig )
    plt.title( title, fontsize=24, loc='right', x=-0.3 )
    plt.savefig( outputFileName, bbox_inches='tight' )
    plt.close()

def plotCondM( ax, numOfPtcsArray, condMArray, label, marker ):
    #fig, ax = plt.subplots( 1, 1, figsize=( 10, 7 ) )
    ax.plot( numOfPtcsArray, condMArray, label=label, marker=next( marker ), linewidth=2, markersize=9  )
    #plt.savefig( outputFileName, bbox_inches='tight' )

def analyseSingleOrder( order,
                        hVect,
                        resolutions,
                        testpointsDistribution,
                        ABFFunction,
                        ABFName,
                        outputFolder,
                        testpointsFolder,
                        plotMatrices = True ):
    numOfPtcsSamples = []
    condMarray = []
    detMarray = []

    for h in hVect:
        for resolution in resolutions:
            boxEdge = 2. * ( 2. * h )
            s = h / resolution
            numOfPtcsPerDim = ( ( int )( boxEdge / s ) ) + 1
            numOfPtcs = numOfPtcsPerDim ** 3
            testpointsInputFileName = testpointsFolder + f"testpoints_" + testpointsDistribution + f"_h_{h:.3f}_resolution_{resolution:.3f}_n_{numOfPtcs}.csv"
            points = np.genfromtxt( testpointsInputFileName, delimiter=',' )
            xptcs = points[ 0, : ]
            yptcs = points[ 1, : ]
            zptcs = points[ 2, : ]
            x0 = 0.
            y0 = 0.
            z0 = 0.
            M = buildMomentrumMatrix( x0, y0, z0, xptcs, yptcs, zptcs, h, order, ABFFunction )
            condM = np.linalg.cond( M )
            detM = np.linalg.det( M )

            if plotMatrices == True:
                title = f'N = {numOfPtcs}, cond(M) = {condM:.3E}, det(M) = {detM:.3E}'
                #title = f'N = {numOfPtcs}, cond($\mathbb{M}$) = {condM:.3E}, det($\mathbb{M}$) = {detM:.3E}'
                #outputPlotName = 'results/' + folder + '/matrices/' + ABFName + '_matrix_order' + str( order ) + '_' + pointsDistribution + '_n' + str( numOfPtcs ) + '.png'
                outputPlotName = outputFolder + 'matrices/' + testpointsDistribution + "/" + ABFName + f'_matrix_distribution-' + testpointsDistribution + f"_order_{order}_h_{h:.3f}_resolution_{resolution:.3f}_n_{numOfPtcs}.png"
                plotMatrixM( M, title, outputPlotName )

            numOfPtcsSamples.append( numOfPtcs )
            condMarray.append( condM )
            detMarray.append( detM )

    return numOfPtcsSamples, condMarray, detMarray

def plotConditionalNumbers( orderNumOfPtcsArray, resolutions, orderCondMArray, h, distribution ):
    marker = itertools.cycle(('o', 'v', 's', '^', '*', 'D', '>', '<', 'p', 'h', 'X', 'd'))
    fig, ax = plt.subplots( 1, 1, figsize=( 11, 8 ) )
    for i in range( 0, len (orders) ):
        label = 'order = ' + str( orders[ i ] )
        plotCondM( ax, orderNumOfPtcsArray[ i ], orderCondMArray[ i ], label, marker )

    #Put this first
    ax_top = ax.twiny()
    #ax_top.set_xticks( resolutions )
    #ax_top.set_xticklabels( resolutions )
    ax_top.set_xlim([ resolutions[ 0 ], resolutions[ -1 ]  ])
    ax_top.set_xlabel( r"resolution $h/s$" )
    #ax_top.grid( color='black', linestyle='--', linewidth=0.5 )

    ax.grid( color='black', linestyle='--', linewidth=0.5 )
    ax.set_ylabel( r'cond$(\mathbb{M}) $ ')
    ax.set_xlabel( r'number of particles $ N $, $ h $ = ' + f'{h}' )
    ax.set_yscale('log')
    leg = ax.legend()
    leg.get_frame().set_edgecolor('k')


    title = f'Distribution: {distribution}, h = {h}'
    plt.title( title, fontsize=24 )

    outputPlotName = resultsFolder + ABFName + '_distribution-' + testpointsDistribution + f'_h{h:.2f}_condM' +  '.png'
    plt.savefig( outputPlotName, bbox_inches='tight' )

def plotDeterminants( orderNumOfPtcsArray, resolutions, orderDetMArray, h, distribution ):
    marker = itertools.cycle(('o', 'v', 's', '^', '*', 'D', '>', '<', 'p', 'h', 'X', 'd'))
    fig, ax = plt.subplots( 1, 1, figsize=( 11, 8 ) )
    for i in range( 0, len (orders) ):
        label = 'order = ' + str( orders[ i ] )
        plotCondM( ax, orderNumOfPtcsArray[ i ], orderDetMArray[ i ], label, marker )

    ax.grid( color='black', linestyle='--', linewidth=0.5 )
    ax.grid( which="minor", color='grey', linestyle='--', linewidth=0.2 )
    ax.set_ylabel( r'log(det$(\mathbb{M}))$ ')
    ax.set_xlabel( r'number of particles $ N $, $ h $ = ' + f'{h}')
    #ax.set_yscale('log')
    ax.set_yscale('symlog')
    ax.legend()
    leg = ax.legend()
    leg.get_frame().set_edgecolor('k')
    ticks = plt.yticks()[0]
    plt.yticks(np.delete(ticks, np.arange(1, ticks.size, 2)))

    ax_top = ax.twiny()
    #ax_top.set_xticks( resolutions )
    #ax_top.set_xticklabels( resolutions )
    ax_top.set_xlim([ resolutions[ 0 ], resolutions[ -1 ] ])
    ax_top.set_xlabel( r"resolution $h/s$" )
    #ax_top.grid( color='black', linestyle='-.', linewidth=0.5 )

    title = f'Distribution: {distribution}, h = {h}'
    plt.title( title, fontsize=24 )

    #This works only for positive values
    #import matplotlib
    #locmin = matplotlib.ticker.LogLocator(base=10.0, subs=np.arange(5, 10) * .1, numticks=100)
    #ax.yaxis.set_minor_locator(locmin)
    import MinorSymLogLocator
    yaxis = plt.gca().yaxis
    yaxis.set_minor_locator(MinorSymLogLocator.MinorSymLogLocator(1e-1))

    outputPlotName = resultsFolder + ABFName + '_distribution-' + testpointsDistribution + f'_h{h:.2f}_detM' +  '.png'
    plt.savefig( outputPlotName, bbox_inches='tight' )


if __name__ == "__main__":

    #resolutions = np.array( [ 0.75, 1., 1.25, 1.5, 1.75, 2., 2.25, 2.5, 3, 3.5, 4, 4.5, 5 ] )
    #resolutions = np.array( [ 0.75, 1., 1.25, 1.5, 1.75, 2., 2.25, 2.5 ] )
    resolutions = np.array( [  0.85, 1., 1.25, 1.5, 1.75 ] )

    #resolutions = np.array( [ 1., 1.25, 1.5, 1.75, 2., 2.25, 2.5, 3, 3.5, 4, 4.5, 5 ] )
    #hVect = np.array( [ 1., 1.25, 1.5, 2, 2.5 ] )
    hVect = np.array( [ 0.75, 0.5, 0.25, 0.1, 0.05, 0.01 ] )

    #hVect = np.array( [ 1. ] )

    resultsFolder = "results3D/"
    #testpointsDistribution = "cartesian"
    #testpointsDistribution = "cartesianPerturbed_esr_0.25"
    testpointsDistribution = "cartesianPerturbed_esr_0.5"
    #testpointsDistribution = "random"
    #testpointsFolder = "testpoints/" + testpointsDistribution + "/"
    testpointsFolder = "testpoints3D_vol2/" + testpointsDistribution + "/"
    plotMatrices = True
    orders = [ 2, 3, 4, 5, 6 ]

    #ABFName = "W_W0Conic"
    #ABFFunction = ABF3D.W_W0Conic
    #resultsFolder = resultsFolder + "W_W0Conic/"

    #ABFName = "W_taylorMonomsWithGaussian"
    #ABFFunction = ABF3D.W_taylorMonomialsWithGaussian
    #resultsFolder = resultsFolder + "W_taylorMonomsWithGaussian/"

    #ABFName = "W_hermiteWithGaussian"
    #ABFFunction = ABF3D.W_hermitePolynomialsWithGaussian
    #resultsFolder = resultsFolder + "W_hermiteWithGaussian/"

    ABFName = "W_hermiteWithWendlandC2"
    ABFFunction = ABF3D.W_hermiteWithWendlandC2
    resultsFolder = resultsFolder + "W_hermiteWithWendlandC2/"

    plt.rcParams.update({
      "text.usetex": True,
      "text.latex.preamble" : r"\usepackage{amsfonts}",
      "font.family": "Times",
      "font.serif" : "Times New Roman",
      "font.size"  : 24,
      "axes.prop_cycle" : plt.cycler( color=plt.cm.plasma( np.linspace( 0, 0.8, len( orders ) ) ) )
    })


    for h in hVect:
        orderNumOfPtcsArray = []
        orderCondMArray = []
        orderDetMArray = []

        orderNumOfPtcsArray.clear()
        orderCondMArray.clear()
        orderDetMArray.clear()

        for order in orders:

            numOfPtcsArray, condMArray, detMArray = analyseSingleOrder( order,
                                                                        #hVect,
                                                                        np.array( [ h ] ),
                                                                        resolutions,
                                                                        testpointsDistribution,
                                                                        ABFFunction,
                                                                        ABFName,
                                                                        resultsFolder,
                                                                        testpointsFolder,
                                                                        plotMatrices )
            print( "Number of particles: ",  numOfPtcsArray )
            print( "Cond(M): ", condMArray )
            print( "Det(M): ", detMArray )
            orderNumOfPtcsArray.append( numOfPtcsArray )
            orderCondMArray.append( condMArray )
            orderDetMArray.append( detMArray )

        #Save data arrays
        outputNamePrefix = resultsFolder + ABFName + '_dataset_distribution-' + testpointsDistribution
        outputNamePoints = outputNamePrefix + f'_h{h:.2f}_numberOfPoints.csv'
        outputNameCondMArray = outputNamePrefix + f'_h{h:.2f}_condMArray.csv'
        outputNameDetMArray = outputNamePrefix + f'_h{h:.2f}_detMArray.csv'

        np.savetxt( outputNamePoints, orderNumOfPtcsArray, delimiter=',' )
        np.savetxt( outputNameCondMArray, orderCondMArray, delimiter=',' )
        np.savetxt( outputNameDetMArray, orderDetMArray, delimiter=',' )

        orderNumOfPtcsArray = np.genfromtxt( outputNamePoints, delimiter=',' )
        orderCondMArray = np.genfromtxt( outputNameCondMArray, delimiter=',' )
        orderDetMArray = np.genfromtxt( outputNameDetMArray, delimiter=',' )

        #plt.rcParams.update({
        #  "font.size"  : 24
        #})

        plotConditionalNumbers( orderNumOfPtcsArray, resolutions, orderCondMArray, h, testpointsDistribution )
        plotDeterminants( orderNumOfPtcsArray, resolutions, orderDetMArray, h, testpointsDistribution )
