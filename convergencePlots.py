#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.legend import Legend #second legend
import itertools
from analyticFunctions import analyticFunction, analyticalFunctionWrapper
from abf import makeFancyNames

def generateYLabelFromDiffOperator( diffOperator ):
    ylabelString = ""
    if diffOperator == "x":
        ylabelString += r'$||L^{x}\phi - \partial \phi / \partial x ||_2$'
    if diffOperator == "y":
        ylabelString += r'$||L^{y}\phi - \partial \phi / \partial y ||_2$'
    if diffOperator == "xx":
        ylabelString += r'$||L^{xx}\phi - \partial \phi^2 / \partial x^2 ||_2$'
    if diffOperator == "xy":
        ylabelString += r'$||L^{xy}\phi - \partial \phi^2 / \partial x \partial y ||_2$'
    if diffOperator == "yy":
        ylabelString += r'$||L^{yy}\phi - \partial \phi^2 / \partial y^2 ||_2$'

    return ylabelString

def differentialOperatorOrderReduction( diffOperator ):
    if diffOperator == "x":
        difReduceFactor = 0
    elif diffOperator == "y":
        difReduceFactor = 0
    elif diffOperator == "xx":
        difReduceFactor = 1
    elif diffOperator == "xy":
        difReduceFactor = 1
    elif diffOperator == "yy":
        difReduceFactor = 1
    else:
        exit( f"plotResults.py - reduceOrder: Invalid differential operator {diffOperator}." )

    return difReduceFactor

def convergencePlots( orders, hLengths, resolution, resolutionPos, diffOperator ):
    marker = itertools.cycle(('o', 'v', 's', '^', '*', 'D', '>', '<', 'p', 'h', 'X', 'd'))
    fig, ax = plt.subplots( 1, 1, figsize=( 11, 8 ) )

    #  collect operator approximation values from the analysis results
    sourceFilesPathPrefix =  f'{analysisResultsPrefix}{abfName}_{distribution}/'
    if distribution == 'cartesian-perturbed':
        sourceFilesPathPrefix += f"cartesian-perturbed_esr_{esr}/"
    errorPerStencilSizes = [] # [ h, orders ]
    for h in hLengths:
        outputNameDxArray = sourceFilesPathPrefix + f'{abfName}_distribution-{distribution}_h{h}_D{diffOperator}Array.csv'
        orderLdErrorArray = np.genfromtxt( outputNameDxArray, delimiter=',' )
        print( "Processing data: ", outputNameDxArray )
        errorPerOrder = orderLdErrorArray[ :, resolutionPos ]

        # compute the difference between value of differential operator and analytical solution
        for i in range( 0, len( errorPerOrder ) ):
            errorPerOrder[ i ] = abs( errorPerOrder[ i ] - analyticalFunctionWrapper( 0, 0, diffOperator ) )
            difReduceFactor = differentialOperatorOrderReduction( diffOperator )
        errorPerStencilSizes.append( errorPerOrder )

    # plot the data
    errorPerStencilSizesMatrix = np.vstack( errorPerStencilSizes )
    for i in range( 0, len ( orders ) ):
        label = 'order = ' + str( orders[ i ] )
        plt.axline( ( hLengths[ -1 ], np.max( errorPerStencilSizesMatrix[ :, i ] ) ), slope=(orders[ i ] - difReduceFactor), color='k', linewidth=0.75, linestyle="--" )
        ax.plot( hLengths, errorPerStencilSizesMatrix[ :, i ], label=label, marker=next( marker ), linewidth=2, markersize=9  )

    ax.grid( color='black', linestyle='--', linewidth=0.5 )
    ax.set_ylabel( generateYLabelFromDiffOperator( diffOperator ) )
    ax.set_xlabel( r'stencil size $ h $ ' )
    ax.set_xscale('log')
    ax.set_yscale('log')
    leg = ax.legend()
    leg.get_frame().set_edgecolor('k')
    title = f'Distribution: {distribution}, '
    if distribution == 'cartesian-perturbed':
        title += r'$\epsilon/s$: ' + f"{esr}"
    title += f'\nABF: {makeFancyNames(abfName)},' + r' $h/s$: ' + f'{resolution}'
    plt.title( title, fontsize=24 )

    outputPlotName = convergenceResultsPath + f'{abfName}_distribution-{distribution}_resolution{resolution:.2f}_Ld{diffOperator}_convergence.png'  #TODO: resultsPath not properlly passed
    plt.savefig( outputPlotName, bbox_inches='tight' )
    plt.close()

def convergencePlotsCompareAbfs( orders, hLengths, resolution, resolutionPos, abfs, diffOperator ):
    marker = itertools.cycle(('o', 'v', 's', '^', '*', 'D', '>', '<', 'p', 'h', 'X', 'd'))
    linestyle = itertools.cycle(('solid', 'dashed', 'dashdot', 'dotted'))
    fig, ax = plt.subplots( 1, 1, figsize=( 11, 8 ) )
    plotLegend = True

    for abfName in abfs:

        analysisResultsPath = f'{analysisResultsPrefix}{abfName}_{distribution}/'
        if distribution == 'cartesian-perturbed':
            analysisResultsPath += f"cartesian-perturbed_esr_{esr}/"
        analysisOutputNamePrefix = analysisResultsPath + abfName + '_distribution-' + distribution

        abfLinestyle = next( linestyle )

        # collect data
        errorPerStencilSizes = [] # h, orders
        for h in hLengths:
            outputNameDxArray = analysisOutputNamePrefix + f'_h{h}_D{diffOperator}Array.csv'
            orderLdErrorArray = np.genfromtxt( outputNameDxArray, delimiter=',' )
            print( "Processing data: ", outputNameDxArray )
            errorPerOrder = orderLdErrorArray[ :, resolutionPos ]

            # compute the difference between value of differential operator and analytical solution
            for i in range( 0, len( errorPerOrder ) ):
                errorPerOrder[ i ] = abs( errorPerOrder[ i ] - analyticalFunctionWrapper( 0, 0, diffOperator ) )
                difReduceFactor = differentialOperatorOrderReduction( diffOperator )
            errorPerStencilSizes.append( errorPerOrder )

        # plot results
        errorPerStencilSizesMatrix = np.vstack( errorPerStencilSizes )
        for i in range( 0, len ( orders ) ):
            label = 'order = ' + str( orders[ i ] )
            if plotLegend == True:
                plt.axline( ( hLengths[ -1 ], np.max( errorPerStencilSizesMatrix[ :, i ] ) ), slope=(orders[ i ] - difReduceFactor), color='k', linewidth=0.75, linestyle="--" )
                ax.plot( hLengths, errorPerStencilSizesMatrix[ :, i ], label=label, marker=next( marker ), linestyle=abfLinestyle, linewidth=2, markersize=9  )
            else:
                ax.plot( hLengths, errorPerStencilSizesMatrix[ :, i ], marker=next( marker ), linestyle=abfLinestyle, linewidth=2, markersize=9  )
        # print legend only for first abf
        plotLegend = False

    #Second legend:
    ax2 = ax.twinx()
    linestyleSecondLegend = itertools.cycle(('solid', 'dashed', 'dashdot', 'dotted')) #TODO: Do this properly
    for i in range( 0, len( abfs ) ):
        ax2.plot( np.NaN, np.NaN, ls=next( linestyleSecondLegend ), label=makeFancyNames( abfs[ i ] ), c='black' )
    ax2.get_yaxis().set_visible(False)
    legAbfs = ax2.legend( loc=2, fontsize=20 )
    legAbfs.get_frame().set_edgecolor('k')

    ax.grid( color='black', linestyle='--', linewidth=0.5 )
    ax.set_ylabel( generateYLabelFromDiffOperator( diffOperator ) )
    ax.set_xlabel( r'stencil size $ h $ ' )
    ax.set_xscale('log')
    ax.set_yscale('log')
    leg = ax.legend( loc=4, fontsize=20 )
    leg.get_frame().set_edgecolor('k')

    title = f'Distribution: {distribution}, '
    if distribution == 'cartesian-perturbed':
        title += r'$\epsilon/s$: ' + f"{esr}"
    title += r' $h/s$: ' + f'{resolution}'

    plt.title( title, fontsize=24 )

    outputPlotName = convergenceComparisonResultsPath #TODO: resultsPath not properlly passed
    for abfName in abfs:
        outputPlotName += abfName + "_"
    outputPlotName += f'distribution-{distribution}_resolution{resolution:.2f}_Ld{diffOperator}_convergence.png'
    plt.savefig( outputPlotName, bbox_inches='tight' )
    plt.close()

def convergencePlotsCompareDistributions( orders, hLengths, resolution, resolutionPos, abfName, distributions, diffOperator ):
    marker = itertools.cycle(('o', 'v', 's', '^', '*', 'D', '>', '<', 'p', 'h', 'X', 'd'))
    linestyle = itertools.cycle(('solid', 'dashed', 'dashdot', 'dotted'))
    fig, ax = plt.subplots( 1, 1, figsize=( 11, 8 ) )

    plotLegend = True
    for distribution in distributions:

        if "cartesian-perturbed" in distribution:
            distributionType = "cartesian-perturbed"
            distributionSubfolder = distribution
        else:
            distributionType = distribution

        analysisResultsPath = f'./results/sources2D/{abfName}_{distributionType}/'
        if 'cartesian-perturbed' in distribution:
            analysisResultsPath += distribution + "/"
        analysisOutputNamePrefix = analysisResultsPath + abfName + '_distribution-' + distributionType

        resolutionLinestyle = next( linestyle )

        # collect data
        errorPerStencilSizes = [] # h, orders
        for h in hLengths:
            outputNameDxArray = analysisOutputNamePrefix + f'_h{h}_D{diffOperator}Array.csv'
            orderLdErrorArray = np.genfromtxt( outputNameDxArray, delimiter=',' )
            print( "Processing data: ", outputNameDxArray )
            errorPerOrder = orderLdErrorArray[ :, resolutionPos ]

            # compute the difference between value of differential operator and analytical solution
            for i in range( 0, len( errorPerOrder ) ):
                errorPerOrder[ i ] = abs( errorPerOrder[ i ] - analyticalFunctionWrapper( 0, 0, diffOperator ) )
                difReduceFactor = differentialOperatorOrderReduction( diffOperator )
            errorPerStencilSizes.append( errorPerOrder )

        # plot results
        errorPerStencilSizesMatrix = np.vstack( errorPerStencilSizes )
        for i in range( 0, len ( orders ) ):
            label = 'order = ' + str( orders[ i ] )
            if plotLegend == True:
                plt.axline( ( hLengths[ -1 ], np.max( errorPerStencilSizesMatrix[ :, i ] ) ), slope=(orders[ i ] - difReduceFactor), color='k', linewidth=0.75, linestyle="--" )
                ax.plot( hLengths, errorPerStencilSizesMatrix[ :, i ], label=label, marker=next( marker ), linestyle=resolutionLinestyle, linewidth=2, markersize=9  )
            else:
                ax.plot( hLengths, errorPerStencilSizesMatrix[ :, i ], marker=next( marker ), linestyle=resolutionLinestyle, linewidth=2, markersize=9  )
        plotLegend = False

    #Second legend:
    ax2 = ax.twinx()
    linestyleSecondLegend = itertools.cycle(('solid', 'dashed', 'dashdot', 'dotted' )) #TODO: Do this properly
    for i in range( 0, len( distributions ) ):
        labelString = distributions[ i ]
        ax2.plot( np.NaN, np.NaN, ls=next( linestyleSecondLegend ), label=labelString, c='black' )
    ax2.get_yaxis().set_visible(False)
    legAbfs = ax2.legend( loc=2, fontsize=20 )
    legAbfs.get_frame().set_edgecolor('k')

    ax.grid( color='black', linestyle='--', linewidth=0.5 )
    ax.set_ylabel( generateYLabelFromDiffOperator( diffOperator ) )
    ax.set_xlabel( r'stencil size $ h $ ' )
    ax.set_xscale('log')
    ax.set_yscale('log')
    leg = ax.legend( loc=4, fontsize=20 )
    leg.get_frame().set_edgecolor('k')

    title = f'ABF: {makeFancyNames(abfName)}, '
    if distribution == 'cartesian-perturbed':
        title += r'$\epsilon/s$: ' + f"{esr}"
    title += r' $h/s$: ' + f'{resolution}'

    plt.title( title, fontsize=24 )

    outputPlotName = convergenceComparisonResultsPath #TODO: resultsPath not properlly passed
    for distribution in distributions:
        outputPlotName += abfName + "_"
    outputPlotName += f'abf-{abfName}_resolution{resolution:.2f}_Ld{diffOperator}_convergence.png'
    plt.savefig( outputPlotName, bbox_inches='tight' )
    plt.close()

def plotConditionalNumbersCompareAbfs( resolutions, h, distribution, abfs ):
    marker = itertools.cycle(('o', 'v', 's', '^', '*', 'D', '>', '<', 'p', 'h', 'X', 'd'))
    linestyle = itertools.cycle(('solid', 'dashed', 'dashdot', 'dotted'))
    fig, ax = plt.subplots( 1, 1, figsize=( 11, 8 ) )
    plotLegend = True

    for abfName in abfs:

        analysisResultsPath = f'{analysisResultsPrefix}{abfName}_{distribution}/'
        if distribution == 'cartesian-perturbed':
            analysisResultsPath += f"cartesian-perturbed_esr_{esr}/"
        analysisOutputNamePrefix = analysisResultsPath + abfName + '_distribution-' + distribution
        pointsFileName = analysisOutputNamePrefix + f'_h{h}_numberOfPoints.csv'
        dataFileName = analysisOutputNamePrefix + f'_h{h}_condMArray.csv'

        orderNumOfPtcsArray = np.genfromtxt( pointsFileName, delimiter=',' )
        orderCondMArray = np.genfromtxt( dataFileName, delimiter=',' )

        abfLinestyle = next( linestyle )
        # plot the data
        for i in range( 0, len (orders) ):
            # sort data with respect to the number of particles
            idx_order =  np.argsort( orderNumOfPtcsArray[ i ] )
            orderNumOfPtcsArray[ i ] = orderNumOfPtcsArray[ i ][ idx_order ]
            orderCondMArray[ i ] = orderCondMArray[ i ][ idx_order ]

            label = 'order = ' + str( orders[ i ] )
            if plotLegend == True:
                ax.plot( orderNumOfPtcsArray[ i ], orderCondMArray[ i ], label=label, marker=next( marker ), linestyle=abfLinestyle, linewidth=2, markersize=9  )
            else:
                ax.plot( orderNumOfPtcsArray[ i ], orderCondMArray[ i ], marker=next( marker ), linestyle=abfLinestyle, linewidth=2, markersize=9  )
        plotLegend = False

    #Second legend:
    ax2 = ax.twinx()
    linestyleSecondLegend = itertools.cycle(('solid', 'dashed', 'dashdot', 'dotted')) #TODO: Do this properly
    for i in range( 0, len( abfs ) ):
        ax2.plot( np.NaN, np.NaN, ls=next( linestyleSecondLegend ), label=makeFancyNames( abfs[ i ] ), c='black' )
    ax2.get_yaxis().set_visible(False)
    legAbfs = ax2.legend( loc=2, fontsize=20 )
    legAbfs.get_frame().set_edgecolor('k')

    #Put this first
    ax_top = ax.twiny()
    ax_top.set_xlim([ resolutions[ 0 ], resolutions[ -1 ]  ])
    ax_top.set_xlabel( r"resolution $h/s$" )
    ax.grid( color='black', linestyle='--', linewidth=0.5 )
    ax.set_ylabel( r'cond$(\mathbb{M}) $ ')
    ax.set_xlabel( r'number of particles $ N $' )
    ax.set_yscale('log')
    leg = ax.legend( fontsize=20 )
    leg.get_frame().set_edgecolor('k')

    title = f'Distribution: {distribution}, '
    if distribution == 'cartesian-perturbed':
        title += r'$\epsilon/s$: ' + f"{esr}, "
    title += f'h = {h}'
    plt.title( title, fontsize=24 )

    outputPlotName = condMComparionResultsPath #TODO: resultsPath not properlly passed
    for abfName in abfs:
        outputPlotName += abfName + "_"
    outputPlotName += f'distribution-{distribution}_h{h}_condM.png'
    plt.savefig( outputPlotName, bbox_inches='tight' )
    plt.close()

def plotConditionalNumbersCompareDistributions( resolutions, h, abfName, distributions ):
    marker = itertools.cycle(('o', 'v', 's', '^', '*', 'D', '>', '<', 'p', 'h', 'X', 'd'))
    linestyle = itertools.cycle(('solid', 'dashed', 'dashdot', 'dotted'))
    fig, ax = plt.subplots( 1, 1, figsize=( 11, 8 ) )
    plotLegend = True

    for distribution in distributions:

        # resolve subfolder in case cartesian-perturbed resolution is used
        if "cartesian-perturbed" in distribution:
            distributionType = "cartesian-perturbed"
            distributionSubfolder = distribution
        else:
            distributionType = distribution
        # resolve results filenames for given data and load the data
        analysisResultsPath = f'{analysisResultsPrefix}{abfName}_{distributionType}/'
        if 'cartesian-perturbed' in distribution:
            analysisResultsPath += distribution + "/"
        analysisOutputNamePrefix = analysisResultsPath + abfName + '_distribution-' + distributionType
        pointsFileName = analysisOutputNamePrefix + f'_h{h}_numberOfPoints.csv'
        dataFileName = analysisOutputNamePrefix + f'_h{h}_condMArray.csv'

        orderNumOfPtcsArray = np.genfromtxt( pointsFileName, delimiter=',' )
        orderCondMArray = np.genfromtxt( dataFileName, delimiter=',' )

        abfLinestyle = next( linestyle )
        # plot the data
        for i in range( 0, len (orders) ):
            # sort data with respect to the number of particles
            idx_order =  np.argsort( orderNumOfPtcsArray[ i ] )
            orderNumOfPtcsArray[ i ] = orderNumOfPtcsArray[ i ][ idx_order ]
            orderCondMArray[ i ] = orderCondMArray[ i ][ idx_order ]

            label = 'order = ' + str( orders[ i ] )
            if plotLegend == True:
                ax.plot( orderNumOfPtcsArray[ i ], orderCondMArray[ i ], label=label, marker=next( marker ), linestyle=abfLinestyle, linewidth=2, markersize=9  )
            else:
                ax.plot( orderNumOfPtcsArray[ i ], orderCondMArray[ i ], marker=next( marker ), linestyle=abfLinestyle, linewidth=2, markersize=9  )
        plotLegend = False

    # plot second legend with distribution styles
    ax2 = ax.twinx()
    linestyleSecondLegend = itertools.cycle( ( 'solid', 'dashed', 'dashdot', 'dotted' ) ) #TODO: Do this properly
    for i in range( 0, len( distributions ) ):
        labelString = distributions[ i ]
        ax2.plot( np.NaN, np.NaN, ls=next( linestyleSecondLegend ), label=labelString, c='black' )
    ax2.get_yaxis().set_visible( False )
    legAbfs = ax2.legend( loc=2, fontsize=20 )
    legAbfs.get_frame().set_edgecolor( 'k' )

    ax_top = ax.twiny()
    ax_top.set_xlim([ resolutions[ 0 ], resolutions[ -1 ]  ])
    ax_top.set_xlabel( r"resolution $h/s$" )
    ax.grid( color='black', linestyle='--', linewidth=0.5 )
    ax.set_ylabel( r'cond$(\mathbb{M}) $ ')
    ax.set_xlabel( r'number of particles $ N $' )
    ax.set_yscale('log')
    leg = ax.legend( fontsize=20 )
    leg.get_frame().set_edgecolor('k')

    title = f'ABF: {makeFancyNames(abfName)}, '
    if distribution == 'cartesian-perturbed':
        title += r'$\epsilon/s$: ' + f"{esr}, "
    title += f'h = {h}'
    plt.title( title, fontsize=24 )

    # compose output file name and save the results
    outputPlotName = condMComparionResultsPath + abfName #TODO: condMComparionResultsPath is not properlly passed
    for distribution in distributions:
        outputPlotName += "_" + distribution
    outputPlotName +=  f'_h{h}_condM' + '.png'

    plt.savefig( outputPlotName, bbox_inches='tight' )
    plt.close()


if __name__ == "__main__":
    import os
    import argparse
    argparser = argparse.ArgumentParser(description="Particle sample sets parameters" )
    g = argparser.add_argument_group("control parameters")
    g.add_argument('-convergence-plots', action='store_true', help="make convergence plots from available data" )
    g.add_argument('-compare-distributions', action='store_true', help="compare multiple distributions" )
    g.add_argument('-compare-abfs', action='store_true', help="compare multiple abfs" )
    g.add_argument('-compare-cond-distributions', action='store_true', help="compare conditional numbers with respect to given resolutions" )
    g.add_argument('-compare-cond-abfs', action='store_true', help="compare conditional numbers with respect to given abfs" )
    g.add_argument('--distributions',  nargs='+', help="distributions to plot in single graph" )
    g.add_argument('--abfs',  nargs='+', help="abfs to plot in single graph" )
    g = argparser.add_argument_group("selection parameters" )
    g.add_argument("--distribution", choices=['random', 'cartesian', 'cartesian-perturbed'], default='cartesian-perturbed', help="particle distribution to generate")
    g.add_argument("--esr", type=float, default=0.5, help="perturbation coefficient for perturbed distributions")
    g = argparser.add_argument_group("selection parameters")
    g.add_argument("--abf", choices=[ 'W0Conic', 'hermiteWithWendlandC2', 'hermiteWithGaussian', 'taylorWithGaussian', "taylorWithWendlandC2" ], default='taylorWithWendlandC2', help="kernel functions to generate")
    g.add_argument('--orders', nargs='+', default=[ 2, 3, 4, 5 ], help='orders to analyze')
    g.add_argument('--resolution', type=float, default=1.5, help='resulution to use for convergence analysis' )
    g.add_argument('--resolutions', nargs='+', default=[ 1., 1.1, 1.25, 1.5, 1.75, 2 ], help='resolutions to generate')
    g.add_argument('--h-lengths', nargs='+', default=[ 0.001, 0.005, 0.01, 0.05, 0.1, 0.5 ] , help='scales to generate') #for the test 0.1 was added

    # parse input arguments
    args = argparser.parse_args()

    abfName = args.abf
    distribution = args.distribution
    esr = args.esr
    resolutions = np.array( args.resolutions, dtype=float )
    hLengths = np.array( args.h_lengths, dtype=float )
    resolution = args.resolution
    #resolutionPos = resolutions.index( resolution )
    resolutionPos, = np.where( resolutions == resolution )[ 0 ]
    orders = np.array( args.orders, dtype=int )
    distributions = args.distributions
    abfs = args.abfs
    listOfDerivativesToCompare = [ "x", "y", "xx", "xy", "yy" ]
    analysisResultsPrefix = r'./results/sources2D/'
    outputResultsPrefix = r'./results/convergenceAndComparison2D/'

    # setup plot properties
    plt.rcParams.update({
      "text.usetex": True,
      "text.latex.preamble" : r"\usepackage{amsfonts}",
      "font.family": "Times",
      "font.serif" : "Times New Roman",
      "font.size"  : 24,
      "axes.prop_cycle" : plt.cycler( color=plt.cm.plasma( np.linspace( 0, 0.8, len( orders ) ) ) )
    })

    # plot convergence for single ABF with single resolution
    if args.compare_abfs:
        convergenceComparisonResultsPath = f'{outputResultsPrefix}comparison-abfs_'  + distribution + "/"
        if distribution == 'cartesian-perturbed':
            convergenceComparisonResultsPath += f"cartesian-perturbed_esr_{esr}/"
        if not os.path.exists( convergenceComparisonResultsPath ):
            os.makedirs( convergenceComparisonResultsPath )

        # for all the tested derivatives, perfrom the comparison
        for derivative in listOfDerivativesToCompare:
            print( f"For distributuion {distribution} compare different ABFs: {abfs}" )
            print( "orders: ", orders, " h-lengths: ", hLengths, " resolutions: ", resolution, " diff. operator: d (  )/d", derivative )
            convergencePlotsCompareAbfs( orders, hLengths, resolution, resolutionPos, abfs, derivative )

    if args.compare_distributions:
        convergenceComparisonResultsPath = f'{outputResultsPrefix}comparison-distributions_'  + abfName + "/"
        if not os.path.exists( convergenceComparisonResultsPath ):
            os.makedirs( convergenceComparisonResultsPath )

        # for all the tested derivatives, perfrom the comparison
        for derivative in listOfDerivativesToCompare:
            print( f"For ABF {abfName} compare different distributions: {distributions}" )
            print( "orders: ", orders, " h-lengths: ", hLengths, " resolutions: ", resolution, " diff. operator: d (  )/d", derivative )
            convergencePlotsCompareDistributions( orders, hLengths, resolution, resolutionPos, abfName, distributions, derivative )

    if args.convergence_plots:
        convergenceResultsPath = f'{outputResultsPrefix}' + abfName + "_" + distribution + "/"
        if distribution == 'cartesian-perturbed':
            convergenceResultsPath += f"cartesian-perturbed_esr_{esr}/"
        if not os.path.exists( convergenceResultsPath ):
            os.makedirs( convergenceResultsPath )

        # for all the tested derivatives, perfrom the comparison
        for derivative in listOfDerivativesToCompare:
            print( "Making convergence plot:" )
            print( "orders: ", orders, " h-lengths: ", hLengths, " resolutions: ", resolution, " diff. operator: d (  )/d", derivative )
            convergencePlots( orders, hLengths, resolution, resolutionPos, derivative )

    if args.compare_cond_abfs:
        print( f"Compare cond(M) for ABFs: {args.compare_cond_abfs}" )
        print( f"For distribution: {distribution} compare ABFs: {abfs}" )
        for h in hLengths:
            condMComparionResultsPath = f'{outputResultsPrefix}comparison-condM-abfs_' + distribution + "/"
            if distribution == 'cartesian-perturbed':
                condMComparionResultsPath += f"cartesian-perturbed_esr_{esr}/"
            if not os.path.exists( condMComparionResultsPath ):
                os.makedirs( condMComparionResultsPath )

            plotConditionalNumbersCompareAbfs( resolutions, h, distribution, abfs )

    if args.compare_cond_distributions:
        print( f"Compare cond(M) for distribution: {args.compare_cond_distributions}" )
        print( f"For ABF: {abfName} compare distributions: {distributions}" )
        for h in hLengths:
            condMComparionResultsPath = f'{outputResultsPrefix}comparison-condM-distributions_' + abfName + "/"
            if distribution == 'cartesian-perturbed':
                condMComparionResultsPath += f"cartesian-perturbed_esr_{esr}/"
            if not os.path.exists( condMComparionResultsPath ):
                os.makedirs( condMComparionResultsPath )

            plotConditionalNumbersCompareDistributions( resolutions, h, abfName, distributions )
