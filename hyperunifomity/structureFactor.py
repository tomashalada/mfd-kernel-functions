#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import glob

def loadPoints( testpointsFolder, testpointsDistribution, h, resolutions ):
    testPointsInputFileNamePattern = testpointsFolder + f"testpoints_" + testpointsDistribution + f"_h_{h:.3f}_resolution_{resolution:.3f}_n_*.csv"

    matchingFiles = glob.glob( testPointsInputFileNamePattern )
    if matchingFiles:
        testPointsInputFileName = matchingFiles[ 0 ]
        print( f"Test points filename: {testPointsInputFileName}" )
        points = np.genfromtxt( testPointsInputFileName, delimiter=',' )
    else:
        print("No matching test points file for patern {testPointsInputFileNamePattern} found.")

    return points

def computeStructureFactor( points, kNumber = 50 ):

    xptcs = points[ 0, : ]
    yptcs = points[ 1, : ]

    nptcs = len( xptcs )
    SkArray = np.zeros( kNumber**2 )
    kSizeArray = np.zeros( kNumber**2 )
    #kValues = np.linspace( kSizeRange[ 0 ], kSizeRange[ 1 ], 50 )

    for n in range( 0, kNumber ):
        for m in range( 0, kNumber ):
            Sk = 0
            kx =  2 * np.pi * n
            ky =  2 * np.pi * m

            for i in range( 0, nptcs ):
                Sk += np.exp( 1j * ( kx * xptcs[ i ] + ky * yptcs[ i ] ) )

            SkArray[ n ] = 1 / nptcs * np.absolute( Sk )**2
            kSizeArray[ n ] = ( kx**2 + ky**2 )**0.5

    idxOrder =  np.argsort( kSizeArray )[ ::-1 ]
    kSizeArray = np.array(  kSizeArray  )[ idxOrder ]
    SkArray = np.array( SkArray  )[ idxOrder ]
    plotStructureFactor( kSizeArray, SkArray, 'a' )

def analyticalStructuralFactor():
    alpha = 0.5
    K = 40 * np.pi
    H = 1e-3

    kNumber = 30

    kSizeArray = []
    S0kArray = []

    for n in range( 0, kNumber ):
        for m in range( 0, kNumber ):
            kx = 2 * np.pi * n
            ky = 2 * np.pi * m

            kSize = ( kx**2 + ky**2 )**0.5
            if kSize < K: S0k = K**( -alpha ) * ( 1 - H ) * kSize**( alpha ) + H
            else: S0k = 1

            kSizeArray.append( kSize )
            S0kArray.append( S0k )

    idxOrder =  np.argsort( kSizeArray )[ ::-1 ]
    kSizeArray = np.array(  kSizeArray  )[ idxOrder ]
    S0kArray = np.array( S0kArray  )[ idxOrder ]
    plotStructureFactor( kSizeArray, S0kArray, 'a' )

def plotStructureFactor( kSizeArray, SkArray, plotOutputName ):
    fig, ax = plt.subplots( 1, 1, figsize=( 10 , 10 ) )
    ax.plot( kSizeArray, SkArray, color='k' )
    ax.grid( color='grey', linestyle='--', linewidth=0.5 )
    plt.locator_params( axis='x', nbins=5 )
    plt.locator_params( axis='y', nbins=5 )
    #plt.savefig( plotOutputName, bbox_inches='tight' )
    #plt.close()
    print("Show")
    plt.show()

def plotPoints( xptcs, yptcs, h, plotOutputName ):
    fig, ax = plt.subplots( 1, 1, figsize=( 10 , 10 ) )
    ax.scatter( xptcs, yptcs, color='k' )
    ax.scatter( 0, 0, color='r' )
    searchRadius = plt.Circle( ( 0, 0 ), 2 *h , color='r', fill = False )
    ax.add_patch( searchRadius )
    ax.grid( color='grey', linestyle='--', linewidth=0.5 )
    plt.locator_params( axis='x', nbins=5 )
    plt.locator_params( axis='y', nbins=5 )
    #plt.savefig( plotOutputName, bbox_inches='tight' )
    #plt.close()
    plt.show()

if __name__ == "__main__":
    import os
    import argparse
    argparser = argparse.ArgumentParser(description="Generate particle samples to perform tests")
    g = argparser.add_argument_group("selection parameters")
    g.add_argument("--distribution", choices=['random', 'cartesian', 'cartesian-perturbed'], default='random', help="particle distribution to generate")
    g.add_argument("--esr", type=float, default=0.5, help="perturbation coefficient for perturbed distributions")
    g.add_argument("--plot", type=bool, default=False, help="plot images of particle samples")
    g = argparser.add_argument_group("parameters of particle configuration")
    #NOTE Use: python generatePoints3D.py -l 0.75 0.85,...
    g.add_argument('--resolutions', nargs='+', default=[ 0.85, 1., 1.1, 1.25, 1.5, 1.75, 2 ], help='resolutions to generate')
    g.add_argument('--h-lengths', nargs='+', default=[ 0.001, 0.005, 0.01, 0.05, 0.1, 0.5 ] , help='scales to generate') #For the test, I added 0.1
    g.add_argument('--samples-count', type=int, default=1, help="number of resolutions to sample" )

    args = argparser.parse_args()

    #parse input arguments
    distribution = args.distribution
    plotOption = args.plot
    esr = args.esr
    resolutions = np.array( args.resolutions, dtype=float )
    hLengths = np.array( args.h_lengths, dtype=float )
    numberOfSamples = args.samples_count

    #setup plot properties
    plt.rcParams.update({
      "text.usetex": True,
      "font.family": "Times",
      "font.serif" : "Times New Roman",
      "font.size"  : 28
    })

    #for every case of the resolution, we want to generate several samples
    for s in range( 0, numberOfSamples ):

        #create folder and subfolder for results
        resultsPath = r'./results/particles2D/' + distribution + "/" + f"sample_{s}/"
        if not os.path.exists( resultsPath ):
            os.makedirs( resultsPath )

        # test all distributions
        distribution = 'cartesian-perturbed'; h = 0.01; resolution = 1.;
        #def loadPoints( testpointsFolder, testpointsDistribution, h, resolutions, numOfPtcs ):
        pointsDataDir = f"../results_2-5/particles2D/cartesian-perturbed/cartesian-perturbed_esr_{esr:.2f}/"
        points =  loadPoints( pointsDataDir, distribution, h, resolution )
        computeStructureFactor( points )

        analyticalStructuralFactor()

        ""
        #generate particle samples
        """
        for h in hLengths:
            for resolution in resolutions:
                boxEdge = 2. * ( 2. * h )

                if distribution == "random":
                    generateRandomDistributions( h, resolution, boxEdge, outputFolder = resultsPath, plot = plotOption  )
                elif distribution == "cartesian":
                    generateCartesianDistributions( h, resolution, boxEdge, outputFolder = resultsPath, plot = plotOption )
                elif distribution == "cartesian-perturbed":
                    #create sub folder for given perturbation coefficient
                    resultsPath = r'./results/particles2D/cartesian-perturbed/' + f"cartesian-perturbed_esr_{esr:.2f}/" + f"sample_{s}/"
                    if not os.path.exists( resultsPath ):
                        os.makedirs( resultsPath )
                    generateCartesianPerturbedDistributions( h, resolution, boxEdge, esr, outputFolder = resultsPath, plot = plotOption )
"""
