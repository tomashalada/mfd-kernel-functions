#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

def plotPoints( xptcs, yptcs, h, plotOutputName ):
    fig, ax = plt.subplots( 1, 1, figsize=( 10 , 10 ) )
    ax.scatter( xptcs, yptcs, color='k' )
    ax.scatter( 0, 0, color='r' )
    searchRadius = plt.Circle( ( 0, 0 ), 2 *h , color='r', fill = False )
    ax.add_patch( searchRadius )
    ax.grid( color='grey', linestyle='--', linewidth=0.5 )
    plt.locator_params( axis='x', nbins=5 )
    plt.locator_params( axis='y', nbins=5 )
    plt.savefig( plotOutputName, bbox_inches='tight' )
    plt.close()

def generateRandomDistributions( h, resolution, boxEdge, outputFolder, plot = False ):
    s = h / resolution
    numOfPtcsPerDim = ( ( int )( boxEdge / s ) ) + 1
    numOfPtcs = numOfPtcsPerDim ** 2
    boxLimits = boxEdge / 2
    xptcs = np.random.uniform( -boxLimits, boxLimits, numOfPtcs )
    yptcs = np.random.uniform( -boxLimits, boxLimits, numOfPtcs )
    points = np.array( [ xptcs, yptcs ] )
    outputName = outputFolder + "testpoints_random" + f"_h_{h:.3f}" + f"_resolution_{resolution:.3f}" + f"_n_{numOfPtcs}" + ".csv"
    np.savetxt( outputName, points, delimiter=',' )

    if plot == True:
        plotOutputName = outputFolder + "fig_testpoints_random" + f"_h_{h:.3f}" + f"_resolution_{resolution:.3f}" + f"_n_{numOfPtcs}" + ".png"
        plotPoints( points[ 0 , : ], points[ 1 , : ], h, plotOutputName )

def generateCartesianDistributions( h, resolution, boxEdge, outputFolder, plot = False ):
    s = h / resolution
    numOfPtcsPerDim = ( ( int )( boxEdge / s ) ) + 1
    numOfPtcs = numOfPtcsPerDim ** 2
    boxLimits = boxEdge / 2
    xcords = np.linspace( -boxLimits, boxLimits, numOfPtcsPerDim )
    ycords = np.linspace( -boxLimits, boxLimits, numOfPtcsPerDim )
    xptcsmesh, yptcsmesh = np.meshgrid( xcords, ycords )
    points = np.vstack( [ xptcsmesh.ravel(), yptcsmesh.ravel() ] )
    outputName = outputFolder + "testpoints_cartesian" + f"_h_{h:.3f}" + f"_resolution_{resolution:.3f}" + f"_n_{numOfPtcs}" + ".csv"
    np.savetxt( outputName, points, delimiter=',' )

    if plot == True:
        plotOutputName = outputFolder + "fig_testpoints_cartesian" + f"_h_{h:.3f}" + f"_resolution_{resolution:.3f}" + f"_n_{numOfPtcs}" + ".png"
        plotPoints( points[ 0 , : ], points[ 1 , : ], h, plotOutputName )

def generateCartesianPerturbedDistributions( h, resolution, boxEdge, esr, outputFolder, plot = False ):
    s = h / resolution
    numOfPtcsPerDim = ( ( int )( boxEdge / s ) ) + 1
    numOfPtcs = numOfPtcsPerDim ** 2
    boxLimits = boxEdge / 2
    xcords = np.linspace( -boxLimits, boxLimits, numOfPtcsPerDim )
    ycords = np.linspace( -boxLimits, boxLimits, numOfPtcsPerDim )
    xptcsmesh, yptcsmesh = np.meshgrid( xcords, ycords )
    points = np.vstack( [ xptcsmesh.ravel(), yptcsmesh.ravel() ] )
    e = esr * s
    points[ 0 , : ] = points[ 0 , : ] + np.random.uniform( -e, e, numOfPtcs )
    points[ 1 , : ] = points[ 1 , : ] + np.random.uniform( -e, e, numOfPtcs )

    points[ 0 , : ] = [ x - 2 * ( x - boxLimits ) if x > boxLimits else x - 2 * ( x + boxLimits ) if x < -boxLimits else x for x in points[ 0 , : ] ]
    points[ 1 , : ] = [ y - 2 * ( y - boxLimits ) if y > boxLimits else y - 2 * ( y + boxLimits ) if y < -boxLimits else y for y in points[ 1 , : ] ]

    outputName = outputFolder + f"testpoints_cartesian-perturbed_h_{h:.3f}_resolution_{resolution:.3f}_n_{numOfPtcs}.csv"
    np.savetxt( outputName, points, delimiter=',' )

    if plot == True:
        plotOutputName = outputFolder + f"fig_testpoints_cartesianPerturbed_esr_{esr:.2}_h_{h:.3f}_resolution_{resolution:.3f}_n_{numOfPtcs}.png"
        plotPoints( points[ 0 , : ], points[ 1 , : ], h, plotOutputName )


if __name__ == "__main__":
    import os
    import argparse
    argparser = argparse.ArgumentParser(description="Generate particle samples to perform tests")
    g = argparser.add_argument_group("selection parameters")
    g.add_argument("--distribution", choices=['random', 'cartesian', 'cartesian-perturbed'], default='random', help="particle distribution to generate")
    g.add_argument("--esr", type=float, default=0.5, help="perturbation coefficient for perturbed distributions")
    g.add_argument("--plot", type=bool, default=False, help="plot images of particle samples")
    g = argparser.add_argument_group("parameters of particle configuration")
    #NOTE Use: python generatePoints3D.py -l 0.75 0.85,...
    g.add_argument('--resolutions', nargs='+', default=[ 0.85, 1., 1.1, 1.25, 1.5, 1.75, 2 ], help='resolutions to generate')
    g.add_argument('--h-lengths', nargs='+', default=[ 0.001, 0.005, 0.01, 0.05, 0.1, 0.5 ] , help='scales to generate') #For the test, I added 0.1
    g.add_argument('--samples-count', type=int, default=1, help="number of resolutions to sample" )

    args = argparser.parse_args()

    #parse input arguments
    distribution = args.distribution
    plotOption = args.plot
    esr = args.esr
    resolutions = np.array( args.resolutions, dtype=float )
    hLengths = np.array( args.h_lengths, dtype=float )
    numberOfSamples = args.samples_count

    #setup plot properties
    plt.rcParams.update({
      "text.usetex": True,
      "font.family": "Times",
      "font.serif" : "Times New Roman",
      "font.size"  : 28
    })

    #for every case of the resolution, we want to generate several samples
    for s in range( 0, numberOfSamples ):

        #create folder and subfolder for results
        resultsPath = r'./results/particles2D/' + distribution + "/" + f"sample_{s}/"
        if not os.path.exists( resultsPath ):
            os.makedirs( resultsPath )

        #generate particle samples
        for h in hLengths:
            for resolution in resolutions:
                boxEdge = 2. * ( 2. * h )

                if distribution == "random":
                    generateRandomDistributions( h, resolution, boxEdge, outputFolder = resultsPath, plot = plotOption  )
                elif distribution == "cartesian":
                    generateCartesianDistributions( h, resolution, boxEdge, outputFolder = resultsPath, plot = plotOption )
                elif distribution == "cartesian-perturbed":
                    #create sub folder for given perturbation coefficient
                    resultsPath = r'./results/particles2D/cartesian-perturbed/' + f"cartesian-perturbed_esr_{esr:.2f}/" + f"sample_{s}/"
                    if not os.path.exists( resultsPath ):
                        os.makedirs( resultsPath )
                    generateCartesianPerturbedDistributions( h, resolution, boxEdge, esr, outputFolder = resultsPath, plot = plotOption )
