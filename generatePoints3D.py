#! /usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

def generateRandomDistributions( h, resolution, boxEdge, outputFolder, plot = False ):
    s = h / resolution
    numOfPtcsPerDim = ( ( int )( boxEdge / s ) ) + 1
    numOfPtcs = numOfPtcsPerDim ** 2
    boxLimits = boxEdge / 2
    xptcs = np.random.uniform( -boxLimits, boxLimits, numOfPtcs )
    yptcs = np.random.uniform( -boxLimits, boxLimits, numOfPtcs )
    zptcs = np.random.uniform( -boxLimits, boxLimits, numOfPtcs )
    points = np.array( [ xptcs, yptcs, zptcs ] )
    outputName = outputFolder + "testpoints_random" + f"_h_{h:.3f}" + f"_resolution_{resolution:.3f}" + f"_n_{numOfPtcs}" + ".csv"
    np.savetxt( outputName, points, delimiter=',' )

def generateCartesianDistributions( h, resolution, boxEdge, outputFolder, plot = False ):
    s = h / resolution
    numOfPtcsPerDim = ( ( int )( boxEdge / s ) ) + 1
    numOfPtcs = numOfPtcsPerDim ** 3
    boxLimits = boxEdge / 2
    xcords = np.linspace( -boxLimits, boxLimits, numOfPtcsPerDim )
    ycords = np.linspace( -boxLimits, boxLimits, numOfPtcsPerDim )
    zcords = np.linspace( -boxLimits, boxLimits, numOfPtcsPerDim )
    xptcsmesh, yptcsmesh, zptcsmesh = np.meshgrid( xcords, ycords, zcords )
    points = np.vstack( [ xptcsmesh.ravel(), yptcsmesh.ravel(), zptcsmesh.ravel() ] )
    outputName = outputFolder + "testpoints_cartesian" + f"_h_{h:.3f}" + f"_resolution_{resolution:.3f}" + f"_n_{numOfPtcs}" + ".csv"
    np.savetxt( outputName, points, delimiter=',' )

def generateCartesianPerturbedDistributions( h, resolution, boxEdge, esr, outputFolder, plot = False ):
    s = h / resolution
    numOfPtcsPerDim = ( ( int )( boxEdge / s ) ) + 1
    numOfPtcs = numOfPtcsPerDim ** 3
    boxLimits = boxEdge / 2
    xcords = np.linspace( -boxLimits, boxLimits, numOfPtcsPerDim )
    ycords = np.linspace( -boxLimits, boxLimits, numOfPtcsPerDim )
    zcords = np.linspace( -boxLimits, boxLimits, numOfPtcsPerDim )
    xptcsmesh, yptcsmesh, zptcsmesh = np.meshgrid( xcords, ycords, zcords )
    points = np.vstack( [ xptcsmesh.ravel(), yptcsmesh.ravel(), zptcsmesh.ravel() ] )
    e = esr * s
    points[ 0 , : ] = points[ 0 , : ] + np.random.uniform( -e, e, numOfPtcs )
    points[ 1 , : ] = points[ 1 , : ] + np.random.uniform( -e, e, numOfPtcs )
    points[ 2 , : ] = points[ 2 , : ] + np.random.uniform( -e, e, numOfPtcs )

    points[ 0 , : ] = [ x - 2 * ( x - boxLimits ) if x > boxLimits else x - 2 * ( x + boxLimits ) if x < -boxLimits else x for x in points[ 0 , : ] ]
    points[ 1 , : ] = [ y - 2 * ( y - boxLimits ) if y > boxLimits else y - 2 * ( y + boxLimits ) if y < -boxLimits else y for y in points[ 1 , : ] ]
    points[ 2 , : ] = [ z - 2 * ( z - boxLimits ) if z > boxLimits else z - 2 * ( z + boxLimits ) if z < -boxLimits else z for z in points[ 2 , : ] ]

    outputName = outputFolder + f"testpoints_cartesianPerturbed_esr_{esr:.2}_h_{h:.3f}_resolution_{resolution:.3f}_n_{numOfPtcs}.csv"
    np.savetxt( outputName, points, delimiter=',' )

if __name__ == "__main__":
    import os
    import argparse
    argparser = argparse.ArgumentParser(description="Generate particle samples to perform tests")
    g = argparser.add_argument_group("selection parameters")
    g.add_argument("--distribution", choices=['random', 'cartesian', 'cartesian-perturbed'], default='random', help="initial distance between particles")
    g.add_argument("--esr", type=float, default=0.5, help="perturbation coefficient for perturbed distributions")
    g.add_argument("--plot", default=False, help="plot images of particle samples")
    g = argparser.add_argument_group("parameters of particle configuration")
    #NOTE Use: python generatePoints3D.py -l 0.75 0.85,...
    g.add_argument('--resolutions', nargs='+', default=[ 0.85, 1., 1.25, 1.5, 1.75 ], help='resolutions to generate')
    g.add_argument('--h-lengths', nargs='+', default=[ 1., 1.25, 1.5, 2, 2.5 ] , help='resolutions to generate')
    #NOTE consider also smaller distances, for example [ 0.75, 0.5, 0.25, 0.1, 0.05, 0.01 ]

    args = argparser.parse_args()

    #parse input arguments
    distribution = args.distribution
    plotOption = args.plot
    esr = args.esr
    resolutions = np.array( args.resolutions )
    hLengths = np.array( args.h_lengths )

    #setup plot properties
    plt.rcParams.update({
      "text.usetex": True,
      "font.family": "Times",
      "font.serif" : "Times New Roman",
      "font.size"  : 28
    })

    #create folder and subfolder for results
    resultsPath = r'./particles3D/' + distribution
    if not os.path.exists( resultsPath ):
        os.makedirs( resultsPath )

    #generate particle samples
    for h in hLengths:
        for resolution in resolutions:
            boxEdge = 2. * ( 2. * h )

            if distribution == "random":
                generateRandomDistributions( h, resolution, boxEdge, plot = plotOption, outputFolder = resultsPath )
            elif distribution == "cartesian":
                generateCartesianDistributions( h, resolution, boxEdge, plot = plotOption, outputFolder = resultsPath )
            elif distribution == "cartesian-perturbed":
                #create sub folder for given perturbation coefficient
                resultsPath = r'./particles3D/cartesian-perturbed/' + f"testpoints3D/cartesianPerturbed_esr_{esr:.2f}/"
                if not os.path.exists( resultsPath ):
                    os.makedirs( resultsPath )
                generateCartesianPerturbedDistributions( h, resolution, boxEdge, esr, plot = plotOption, outputFolder = resultsPath )
